const express = require('express');
var crypto = require("crypto");
const path = require("path");
const fs = require("fs");
const moment = require('moment');

const app = express();
const privateKey = fs.readFileSync(path.join(__dirname, 'assets', 'ca_private_key.pem'), "utf8");
const validity = 30;

app.set('json spaces', 2);

app.get('/generateCertificate', function(req, res) {
  generateCertificate(req, res, "prolog");
});

app.get('/generateCertificateJSON', function(req, res) {
  generateCertificate(req, res, "json");
});

app.listen(3000, function () {
  console.log('CA listening on port 3000!');
});

function generateCertificate(req, res, type) {
  if(Object.keys(req.query).length === 2 && req.query.role && req.query.publicKey) {
    const role = req.query.role;
    const publicKey = req.query.publicKey;

    const address = (role == "node" ? "n" : "u") + crypto.createHash('sha256').update(publicKey).digest('hex');
    const expirationInstant = moment().add(validity, 'days').valueOf();

    const toSign = address + publicKey + role + expirationInstant;
    const sign = crypto.createSign('RSA-SHA256');
    sign.update(toSign);
    const signature = sign.sign(privateKey, 'base64');
    var response;
    if(type === "json") {
      response = {
        entityAddress : address,
        publicKey : publicKey,
        role: role,
        expirationInstant: expirationInstant,
        signature: signature
      }
      res.json(response);
    } else {
      response = "certificate(entity_address(" + address + "), public_key('" + publicKey + "'), role(" + role + "), expiration_instant(" + expirationInstant + "), signature('" + signature + "'))";
      res.send(response);
    }
  } else {
    res.send("Wrong get parameters");
  }
}
