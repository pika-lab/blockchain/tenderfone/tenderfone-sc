package org.amaffi.thesis.util;

import org.amaffi.thesis.abci.request.RestClient;
import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.transaction.interfaces.Transaction;
import org.amaffi.thesis.transaction.model.TransactionFactory;
import org.apache.http.HttpResponse;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;

public class CommunicationUtilities {
    private static final String RESULT_KEY = "result";
    private static final String CHECK_TX_KEY = "check_tx";
    private static final String DELIVER_TX_KEY = "deliver_tx";
    private static final String CODE_KEY = "code";

    private static final FutureCallback<HttpResponse> DEFAULT_HANDLER = new FutureCallback<HttpResponse>() {
        @Override
        public void completed(final HttpResponse httpResponse) {
            if(!hasTransactionSucceeded(httpResponse)) {
                NonceManager.getInstance().decrement();
            }
        }

        @Override
        public void failed(final Exception e) {
            NonceManager.getInstance().decrement();
        }

        @Override
        public void cancelled() {
            NonceManager.getInstance().decrement();
        }
    };

    private CommunicationUtilities() {}

    public static boolean hasTransactionSucceeded(final HttpResponse httpResponse) {
        try {
            final String response = EntityUtils.toString(httpResponse.getEntity());
            final JSONObject jsonResult = new JSONObject(response).getJSONObject(RESULT_KEY);
            final JSONObject jsonCheckTx = jsonResult.getJSONObject(CHECK_TX_KEY);
            final JSONObject jsonDeliverTx = jsonResult.getJSONObject(DELIVER_TX_KEY);
            return jsonCheckTx.isNull(CODE_KEY) && jsonDeliverTx.isNull(CODE_KEY);
        } catch (final IOException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    public static void auditTransaction(final Transaction maliciousTx, final ErrorType errorType) {
        final Transaction toAudit = TransactionFactory.buildAuditTransaction(maliciousTx, errorType);
        RestClient.getInstance().asyncBroadcast(toAudit, DEFAULT_HANDLER);
    }
}
