package org.amaffi.thesis.util;

import com.google.common.hash.Hashing;

import org.amaffi.thesis.sc.interfaces.SmartContract;

import java.nio.charset.StandardCharsets;

import java.util.ArrayList;
import java.util.List;

public class Utilities {
    private Utilities() {}

    public static String sha256(final String toHash) {
        return Hashing.sha256().hashString(toHash, StandardCharsets.UTF_8).toString();
    }

    public static String getMerkleTreeRoot(final List<SmartContract> smartContracts) {
        final List<String> hashedData = new ArrayList<>();
        for (final SmartContract smartContract : smartContracts) {
            hashedData.add(sha256(smartContract.getTheory().toString()));
        }
        return calculateTreeRoot(hashedData);
    }

    private static String calculateTreeRoot(final List<String> hashes) {
        if(hashes.isEmpty()) {
            return "";
        } else if(hashes.size() == 1) {
            return hashes.get(0);
        } else if(hashes.size() % 2 == 1) {
            final String partialTreeRootHash = calculateTreeRoot(hashes.subList(0, hashes.size() - 1));
            return sha256(partialTreeRootHash + hashes.get(hashes.size() - 1));
        } else {
            final List<String> intermediateHashes = new ArrayList<>();
            for(int i = 0; i < hashes.size(); i += 2) {
                intermediateHashes.add(sha256(hashes.get(i) + hashes.get(i + 1)));
            }
            return calculateTreeRoot(intermediateHashes);
        }
    }
}
