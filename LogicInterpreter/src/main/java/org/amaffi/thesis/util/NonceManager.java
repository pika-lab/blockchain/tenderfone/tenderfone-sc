package org.amaffi.thesis.util;

public class NonceManager {
    private static final NonceManager INSTANCE = new NonceManager();

    private long appNonce;

    private NonceManager() {}

    public static NonceManager getInstance() {
        return INSTANCE;
    }

    public long getAppNonce() {
        return appNonce++;
    }

    public void decrement() {
        appNonce--;
    }
}
