package org.amaffi.thesis.util;

import java.time.Instant;

public final class TimeManager {
    private static final TimeManager INSTANCE = new TimeManager();

    private volatile Instant globalTime;

    private TimeManager() {}

    public static TimeManager getInstance() {
        return INSTANCE;
    }

    public void updateGlobalTime(final long globalTime) {
        this.globalTime = Instant.ofEpochSecond(globalTime);
    }

    public Instant getGlobalTime() {
        return globalTime;
    }
}
