package org.amaffi.thesis.util;

public final class BlockchainManager {
    private final static BlockchainManager INSTANCE = new BlockchainManager();

    private int validatorsNumber;

    private BlockchainManager() { }

    public static BlockchainManager getInstance() {
        return INSTANCE;
    }

    public void initialize(final int validatorsNumber) {
        if(this.validatorsNumber == 0 && validatorsNumber > 0) {
            this.validatorsNumber = validatorsNumber;
        }
    }

    public int getValidatorsNumber() {
        return validatorsNumber;
    }
}
