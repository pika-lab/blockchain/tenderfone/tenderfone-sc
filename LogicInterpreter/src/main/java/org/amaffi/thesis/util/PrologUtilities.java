package org.amaffi.thesis.util;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;
import alice.tuprolog.exceptions.InvalidTheoryException;
import org.amaffi.thesis.transaction.interfaces.Transaction;

import java.util.Iterator;
import java.util.Optional;

public class PrologUtilities {
    private PrologUtilities() {}

    public static Optional<Theory> theoryFromStruct(final Struct structTheory) {
        final StringBuilder stringTheory = new StringBuilder();
        final Iterator<? extends Term> iterator = ((Struct) structTheory.getArg(0)).listIterator();
        iterator.forEachRemaining(term -> stringTheory.append(term).append(". "));
        try {
            return Optional.of(Theory.parse(stringTheory.toString()));
        } catch (final InvalidTheoryException e) {
            return Optional.empty();
        }
    }

    public static Struct transactionToStruct(final Transaction transaction) {
        return (Struct) Term.createTerm(transaction.toString());
    }
}
