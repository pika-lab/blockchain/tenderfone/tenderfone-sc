package org.amaffi.thesis.abci;

public enum ConsensusPhase {
    CHECK_TX, DELIVER_TX
}
