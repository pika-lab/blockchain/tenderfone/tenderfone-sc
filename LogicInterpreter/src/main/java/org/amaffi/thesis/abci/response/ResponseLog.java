package org.amaffi.thesis.abci.response;

enum ResponseLog {
    MALFORMED_TRANSACTION("Malformed transaction."),
    MALFORMED_CERTIFICATE("Malformed certificate."),
    NO_ID_FOUND("Can't find target smart contract."),
    BAD_NONCE("Bad nonce."),
    INVALID_TRANSACTION_SIGNATURE("Invalid transaction signature."),
    INVALID_CERTIFICATE_SIGNATURE("Invalid certificate signature."),
    CERTIFICATE_EXPIRED("Certificate expired."),
    UNAUTHORIZED("You lack the necessary permissions to execute this transaction."),
    CREATION_TIMEOUT("Smart contract \"on_start\" timed out."),
    INVOCATION_TIMEOUT("Invocation timed out."),
    APP_ERROR("App is broken."),

    VALID_TRANSACTION("Transaction is valid!"),
    CREATION_TRANSACTION_EXECUTED("Smart contract created!"),
    INVOCATION_TRANSACTION_SUCCEEDED("yes."),
    INVOCATION_TRANSACTION_FAILED("no."),
    MULTICAST_OR_BROADCAST_INVOCATION_SUCCEEDED("Multicast/broadcast succeeded"),
    UPDATE_TRANSACTION_SUCCEEDED("Smart contract updated!"),
    UPDATE_TRANSACTION_FAILED("Cannot retract this term!"),
    DESTRUCTION_TRANSACTION_EXECUTED("Smart contract killed!"),
    FORGED_TRANSACTION("Your forged this transaction.");

    private final String message;

    ResponseLog(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
