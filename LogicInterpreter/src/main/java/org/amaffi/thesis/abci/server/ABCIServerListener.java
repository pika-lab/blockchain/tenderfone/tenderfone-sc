package org.amaffi.thesis.abci.server;

import com.github.jtendermint.jabci.types.*;

public interface ABCIServerListener {
    ResponseCheckTx onCheckTx(RequestCheckTx requestCheckTx);
    ResponseBeginBlock onBeginBlock(RequestBeginBlock requestBeginBlock);
    ResponseDeliverTx onDeliverTx(RequestDeliverTx requestDeliverTx);
    ResponseEndBlock onEndBlock(RequestEndBlock requestEndBlock);
    ResponseCommit onCommit(RequestCommit requestCommit);
    ResponseQuery onQuery(RequestQuery requestQuery);
    ResponseInitChain onInitChain(RequestInitChain requestInitChain);
}
