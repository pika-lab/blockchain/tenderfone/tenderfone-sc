package org.amaffi.thesis.abci.server;

import com.github.jtendermint.jabci.api.*;
import com.github.jtendermint.jabci.socket.TSocket;
import com.github.jtendermint.jabci.types.*;

public class ABCIServer implements ICheckTx, IBeginBlock, IDeliverTx, IEndBlock, ICommit, IQuery, IInitChain {

    private static final Integer ABCI_SERVER_PORT = 26658;

    private TSocket socket;
    private ABCIServerListener listener;

    public ABCIServer(final ABCIServerListener listener) {
        this.socket = new TSocket();
        this.socket.registerListener(this);
        this.listener = listener;
    }

    public void start() {
        final Thread thread = new Thread(() -> this.socket.start(ABCI_SERVER_PORT));
        thread.start();
    }

    @Override
    public ResponseCheckTx requestCheckTx(final RequestCheckTx requestCheckTx) {
        return this.listener.onCheckTx(requestCheckTx);
    }

    @Override
    public ResponseBeginBlock requestBeginBlock(RequestBeginBlock requestBeginBlock) {
        return this.listener.onBeginBlock(requestBeginBlock);
    }

    @Override
    public ResponseDeliverTx receivedDeliverTx(final RequestDeliverTx requestDeliverTx) {
        return this.listener.onDeliverTx(requestDeliverTx);
    }

    @Override
    public ResponseEndBlock requestEndBlock(RequestEndBlock requestEndBlock) {
        return this.listener.onEndBlock(requestEndBlock);
    }

    @Override
    public ResponseCommit requestCommit(final RequestCommit requestCommit) {
        return this.listener.onCommit(requestCommit);
    }

    @Override
    public ResponseQuery requestQuery(final RequestQuery requestQuery) {
        return this.listener.onQuery(requestQuery);
    }

    @Override
    public ResponseInitChain requestInitChain(RequestInitChain requestInitChain) {
        return this.listener.onInitChain(requestInitChain);
    }
}
