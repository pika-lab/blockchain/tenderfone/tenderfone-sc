package org.amaffi.thesis.abci.response;

import alice.tuprolog.*;
import alice.tuprolog.exceptions.InvalidTermException;
import alice.tuprolog.exceptions.NoSolutionException;
import com.github.jtendermint.jabci.api.CodeType;
import com.github.jtendermint.jabci.types.*;
import com.google.protobuf.ByteString;
import org.amaffi.thesis.sc.interfaces.SmartContract;
import org.amaffi.thesis.sc.model.SCManager;
import org.amaffi.thesis.transaction.result.*;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

public class ResponseBuilder {
    private ResponseBuilder() {}

    public static ResponseCheckTx buildCheckTxResponse() {
        return ResponseCheckTx.newBuilder().setCode(CodeType.OK)
                .setLog(ResponseLog.VALID_TRANSACTION.toString()).build();
    }

    public static ResponseCheckTx buildCheckTxErrorResponse(final ErrorType errorType) {
        final ResponseCheckTx.Builder response = ResponseCheckTx.newBuilder().setCode(errorType.getErrorCode());
        switch (errorType) {
            case NO_ID_FOUND:
                response.setLog(ResponseLog.NO_ID_FOUND.toString());
                break;
            case MALFORMED_TRANSACTION:
                response.setLog(ResponseLog.MALFORMED_TRANSACTION.toString());
                break;
            case MALFORMED_CERTIFICATE:
                response.setLog(ResponseLog.MALFORMED_CERTIFICATE.toString());
                break;
            case BAD_NONCE:
                response.setLog(ResponseLog.BAD_NONCE.toString());
                break;
            case INVALID_TRANSACTION_SIGNATURE:
                response.setLog(ResponseLog.INVALID_TRANSACTION_SIGNATURE.toString());
                break;
            case INVALID_CERTIFICATE_SIGNATURE:
                response.setLog(ResponseLog.INVALID_CERTIFICATE_SIGNATURE.toString());
                break;
            case CERTIFICATE_EXPIRED:
                response.setLog(ResponseLog.CERTIFICATE_EXPIRED.toString());
                break;
            case CREATION_TIMEOUT:
                response.setLog(ResponseLog.CREATION_TIMEOUT.toString());
                break;
            case INVOCATION_TIMEOUT:
                response.setLog(ResponseLog.INVOCATION_TIMEOUT.toString());
                break;
            case UNAUTHORIZED:
                response.setLog(ResponseLog.UNAUTHORIZED.toString());
                break;
            case FORGED_TRANSACTION:
                response.setLog(ResponseLog.FORGED_TRANSACTION.toString());
                break;
            case APP_ERROR:
                response.setLog(ResponseLog.APP_ERROR.toString());
                break;
        }
        return response.build();
    }

    public static ResponseDeliverTx buildDeliverTxResponse(final TransactionResult result) {
        return result instanceof CreationResult ? buildDeliverTxResponse((CreationResult) result) :
                result instanceof InvocationResult ? buildDeliverTxResponse((InvocationResult) result) :
                        result instanceof UpdateResult ? buildDeliverTxResponse((UpdateResult) result) :
                        buildDeliverTxResponse((DestructionResult) result);
    }

    public static ResponseDeliverTx buildDeliverTxErrorResponse(final ErrorType errorType) {
        ResponseDeliverTx.Builder response = ResponseDeliverTx.newBuilder().setCode(errorType.getErrorCode());
        switch (errorType) {
            case NO_ID_FOUND:
                response.setLog(ResponseLog.NO_ID_FOUND.toString());
                break;
            case MALFORMED_TRANSACTION:
                response.setLog(ResponseLog.MALFORMED_TRANSACTION.toString());
                break;
            case MALFORMED_CERTIFICATE:
                response.setLog(ResponseLog.MALFORMED_CERTIFICATE.toString());
                break;
            case BAD_NONCE:
                response.setLog(ResponseLog.BAD_NONCE.toString());
                break;
            case INVALID_TRANSACTION_SIGNATURE:
                response.setLog(ResponseLog.INVALID_TRANSACTION_SIGNATURE.toString());
                break;
            case INVALID_CERTIFICATE_SIGNATURE:
                response.setLog(ResponseLog.INVALID_CERTIFICATE_SIGNATURE.toString());
                break;
            case CERTIFICATE_EXPIRED:
                response.setLog(ResponseLog.CERTIFICATE_EXPIRED.toString());
                break;
            case CREATION_TIMEOUT:
                response.setLog(ResponseLog.CREATION_TIMEOUT.toString());
                break;
            case INVOCATION_TIMEOUT:
                response.setLog(ResponseLog.INVOCATION_TIMEOUT.toString());
                break;
            case UNAUTHORIZED:
                response.setLog(ResponseLog.UNAUTHORIZED.toString());
                break;
            case FORGED_TRANSACTION:
                response.setLog(ResponseLog.FORGED_TRANSACTION.toString());
                break;
            case APP_ERROR:
                response.setLog(ResponseLog.APP_ERROR.toString());
                break;
        }
        return response.build();
    }

    public static ResponseQuery buildQueryResponse(final RequestQuery requestQuery) {
        try {
            final Struct query = (Struct) Term.createTerm(new String(requestQuery.getData().toByteArray(), StandardCharsets.UTF_8));
            switch (query.getName()) {
                case "smartContractAddresses":
                    final List<SmartContract> smartContracts = SCManager.getInstance().getAllSmartContracts();
                    final StringBuilder builder = new StringBuilder();
                    for (final SmartContract smartContract : smartContracts) {
                        builder.append(smartContract.getAddress()).append("\n");
                    }
                    return buildQueryPositiveResponse(builder.toString());
                case "smartContract":
                    if (query.getArity() == 1) {
                        return buildSmartContractQueryResponse(query.getArg(0).toString());
                    } else {
                        return buildQueryErrorResponse();
                    }
                case "auditedTransactions":
                    return buildSmartContractQueryResponse(SCManager.AUDITOR_ADDRESS);
                default:
                    return buildQueryErrorResponse();
            }
        } catch (final InvalidTermException exception) {
            return buildQueryErrorResponse();
        }
    }

    private static ResponseDeliverTx buildDeliverTxResponse(final CreationResult creationResult) {
        return ResponseDeliverTx.newBuilder().setCode(CodeType.OK)
                .setLog(ResponseLog.CREATION_TRANSACTION_EXECUTED.toString())
                .setData(ByteString.copyFrom(creationResult.getNewSmartContractAddress(), StandardCharsets.UTF_8))
                .build();
    }

    private static ResponseDeliverTx buildDeliverTxResponse(final InvocationResult invocationResult) {
        final Optional<SolveInfo> solveInfo = invocationResult.getSolveInfo();
        final ResponseDeliverTx.Builder response = ResponseDeliverTx.newBuilder().setCode(CodeType.OK);
        if(solveInfo.isPresent()) {
            final SolveInfo actualSolveInfo = solveInfo.get();
            if (actualSolveInfo.isSuccess()) {
                try {
                    response.setLog(ResponseLog.INVOCATION_TRANSACTION_SUCCEEDED.toString())
                            .setData(ByteString.copyFrom(actualSolveInfo.getSolution().toString(), StandardCharsets.UTF_8));
                } catch (final NoSolutionException e) {
                    e.printStackTrace();
                }
            } else {
                response.setLog(ResponseLog.INVOCATION_TRANSACTION_FAILED.toString());
            }
        } else if(!invocationResult.isSingleInvocation()) {
            response.setLog(ResponseLog.MULTICAST_OR_BROADCAST_INVOCATION_SUCCEEDED.toString());
        }
        return response.build();
    }

    private static ResponseDeliverTx buildDeliverTxResponse(final UpdateResult updateResult) {
        final ResponseDeliverTx.Builder response = ResponseDeliverTx.newBuilder().setCode(CodeType.OK);
        final SolveInfo solveInfo = updateResult.getSolveInfo();
        if (solveInfo.isSuccess()) {
            try {
                response.setLog(ResponseLog.UPDATE_TRANSACTION_SUCCEEDED.toString())
                        .setData(ByteString.copyFrom(solveInfo.getSolution().toString(), StandardCharsets.UTF_8));
            } catch (final NoSolutionException e) {
                e.printStackTrace();
            }
        } else {
            response.setLog(ResponseLog.UPDATE_TRANSACTION_FAILED.toString())
                    .setData(ByteString.copyFrom(ResponseLog.UPDATE_TRANSACTION_FAILED.toString()
                            , StandardCharsets.UTF_8));
        }
        return response.build();
    }

    private static ResponseDeliverTx buildDeliverTxResponse(final DestructionResult destructionResult) {
        return ResponseDeliverTx.newBuilder().setCode(CodeType.OK)
                .setLog(ResponseLog.DESTRUCTION_TRANSACTION_EXECUTED.toString())
                .setData(ByteString.copyFrom(destructionResult.getSmartContractAddress(), StandardCharsets.UTF_8))
                .build();
    }

    private static ResponseQuery buildSmartContractQueryResponse(final String address) {
        final Optional<SmartContract> smartContract =
                SCManager.getInstance().getSmartContract(address);
        return smartContract
                .map(logicalSmartContract -> buildQueryPositiveResponse(logicalSmartContract.getTheory().toString()))
                .orElseGet(() -> ResponseQuery.newBuilder()
                    .setCode(CodeType.BAD).setLog(ResponseLog.NO_ID_FOUND.toString()).build());
    }

    private static ResponseQuery buildQueryPositiveResponse(final String data) {
        return ResponseQuery.newBuilder()
                .setCode(CodeType.OK)
                .setValue(ByteString.copyFrom(data, StandardCharsets.UTF_8))
                .build();
    }

    private static ResponseQuery buildQueryErrorResponse() {
        return ResponseQuery.newBuilder().setCode(CodeType.BAD).setLog("Malformed query").build();
    }

}
