package org.amaffi.thesis.abci.request;

import org.amaffi.thesis.security.authorization.Role;
import org.amaffi.thesis.exception.communication.BadCertificationAuthorityResponseException;
import org.amaffi.thesis.security.KeysManager;
import org.amaffi.thesis.transaction.interfaces.Transaction;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.concurrent.FutureCallback;

import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Singleton class used for interaction purposes. It exposes a method for requiring a certificate
 * and a method for broadcasting transaction to the (local) Tendermint core.
 *
 * @author Alfredo Maffi
 */
public final class RestClient {
    private static final String ASYNC_BROADCAST_ADDRESS = "http://core:26657/broadcast_tx_commit";
    private static final String CERTIFICATE_REQUEST_ADDRESS = "http://ca:3000/generateCertificate";
    private static final String ROLE_PARAMETER_NAME = "role";
    private static final String PUBLIC_KEY_PARAMETER_NAME = "publicKey";

    private static RestClient INSTANCE = new RestClient();

    private RestClient() { }

    public static RestClient getInstance() {
        return INSTANCE;
    }

    /**
     * Method used to require a brand new certificate to the certification authority.
     * @return The certificate in a prolog-fashion, as a string object.
     * @throws BadCertificationAuthorityResponseException if the certificate received is not a prolog struct.
     * @throws IOException if the certification authority is unreachable.
     */
    public String requestCertificate() throws BadCertificationAuthorityResponseException, IOException {
        final HttpClient client = HttpClients.createDefault();
        final String role = Role.NODE.toString().toLowerCase();
        final String publicKey = new String(Base64.encodeBase64(KeysManager.getInstance().getAppPublicKey().getEncoded()));
        try {
            final String listStubsUri = new URIBuilder(CERTIFICATE_REQUEST_ADDRESS)
                    .addParameter(ROLE_PARAMETER_NAME, role)
                    .addParameter(PUBLIC_KEY_PARAMETER_NAME, publicKey).build().toString();
            final HttpResponse response = client.execute(new HttpGet(listStubsUri));
            return EntityUtils.toString(response.getEntity());
        } catch (final URISyntaxException exception) {
            return "";
        }
    }

    /**
     * Method used to broadcast a transaction to the local Tendermint core.
     * @param transaction the transaction to be broadcast.
     * @param sender the sender to be notified asynchronously when a response from the core is received.
     */
    public void asyncBroadcast(final Transaction transaction, final FutureCallback<HttpResponse> sender) {
        try {
            final CloseableHttpAsyncClient client = HttpAsyncClients.createDefault();
            client.start();
            final String parameterTx = "\"" + transaction.toString() + "\"";
            final String listStubsUri =
                    new URIBuilder(ASYNC_BROADCAST_ADDRESS).addParameter("tx", parameterTx).build().toString();
            client.execute(new HttpGet(listStubsUri), sender);
        } catch (final URISyntaxException exception) {
            exception.printStackTrace();
        }
    }
}
