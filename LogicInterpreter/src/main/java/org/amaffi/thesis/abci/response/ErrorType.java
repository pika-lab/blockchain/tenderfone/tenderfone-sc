package org.amaffi.thesis.abci.response;

public enum ErrorType {
    MALFORMED_TRANSACTION(1),
    MALFORMED_CERTIFICATE(2),
    NO_ID_FOUND(3),
    BAD_NONCE(4),
    INVALID_TRANSACTION_SIGNATURE(5),
    INVALID_CERTIFICATE_SIGNATURE(6),
    CERTIFICATE_EXPIRED(7),
    UNAUTHORIZED(8),
    CREATION_TIMEOUT(9),
    INVOCATION_TIMEOUT(10),
    APP_ERROR(11),
    FORGED_TRANSACTION(12);

    private final Integer errorCode;

    ErrorType(final Integer errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return this.errorCode;
    }
}
