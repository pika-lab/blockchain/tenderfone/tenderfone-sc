package org.amaffi.thesis.security.certificate.model;

import alice.tuprolog.Number;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import org.amaffi.thesis.security.authorization.Role;
import org.amaffi.thesis.exception.transaction.MalformedCertificateException;
import org.amaffi.thesis.security.KeysManager;
import org.amaffi.thesis.security.certificate.interfaces.Certificate;
import org.apache.commons.codec.binary.Base64;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.time.Instant;

import static org.amaffi.thesis.security.certificate.syntax.CertificateSyntax.*;

public class CertificateFactory {

    private CertificateFactory() { }

    public static Certificate buildCertificate(final String stringCertificate) throws MalformedCertificateException {
        try {
            final Struct certificate = (Struct) Term.createTerm(stringCertificate);
            if(certificate.getName().equals(CERTIFICATE_HEAD) && certificate.getArity() == CERTIFICATE_ARITY
                    && areArgumentsValid(certificate)) {
                final Struct entityAddressStruct = (Struct) certificate.getArg(ENTITY_ADDRESS_STRUCT_POSITION);
                final Struct publicKeyStruct = (Struct) certificate.getArg(PUBLIC_KEY_STRUCT_POSITION);
                final String base64PublicKey = publicKeyStruct.getArg(PUBLIC_KEY_STRUCT_CONTENT_POSITION).toString()
                        .replaceAll("'", "");
                final Struct roleStruct = (Struct)certificate.getArg(ROLE_STRUCT_POSITION);
                final String roleString = roleStruct.getArg(ROLE_STRUCT_CONTENT_POSITION).toString();
                final Struct expirationInstantStruct = (Struct) certificate.getArg(EXPIRATION_INSTANT_STRUCT_POSITION);
                final Long expirationInstantEpoch =
                        ((Number) expirationInstantStruct.getArg(EXPIRATION_INSTANT_STRUCT_CONTENT_POSITION)).longValue();
                final Struct signatureStruct = (Struct) certificate.getArg(SIGNATURE_STRUCT_POSITION);


                final String entityAddress = entityAddressStruct.getArg(ENTITY_ADDRESS_STRUCT_CONTENT_POSITION).toString();
                final PublicKey publicKey = publicKeyFromString(base64PublicKey);
                final Role role = Role.valueOf(roleString.toUpperCase());
                final Instant expirationInstant = Instant.ofEpochSecond(expirationInstantEpoch);
                final String signature = signatureStruct.getArg(SIGNATURE_STRUCT_CONTENT_POSITION)
                        .toString().replaceAll("'", "");

                return new ConcreteCertificate(entityAddress, publicKey, role, expirationInstant, signature);
            }
        } catch ( IllegalArgumentException | NoSuchAlgorithmException | InvalidKeySpecException ignored) { }

        throw new MalformedCertificateException();
    }

    private static boolean areArgumentsValid(final Struct certificateStruct) {
        final Struct entityAddressStruct = (Struct) certificateStruct.getArg(ENTITY_ADDRESS_STRUCT_POSITION);
        final Struct publicKeyStruct = (Struct) certificateStruct.getArg(PUBLIC_KEY_STRUCT_POSITION);
        final Struct roleStruct = (Struct) certificateStruct.getArg(ROLE_STRUCT_POSITION);
        final Struct expirationInstantStruct =  (Struct) certificateStruct.getArg(EXPIRATION_INSTANT_STRUCT_POSITION);
        final Struct signatureStruct = (Struct) certificateStruct.getArg(SIGNATURE_STRUCT_POSITION);
        return isEntityAddressValid(entityAddressStruct) && isPublicKeyValid(publicKeyStruct) && isRoleValid(roleStruct) &&
                isExpirationInstantValid(expirationInstantStruct) && isSignatureValid(signatureStruct);
    }

    private static boolean isEntityAddressValid(final Struct entityAddress) {
        return entityAddress.getName().equals(ENTITY_ADDRESS_STRUCT_HEAD) && entityAddress.getArity() == ENTITY_ADDRESS_STRUCT_ARITY;
    }

    private static boolean isPublicKeyValid(final Struct publicKey) {
        return publicKey.getName().equals(PUBLIC_KEY_STRUCT_HEAD) && publicKey.getArity() == PUBLIC_KEY_STRUCT_ARITY;
    }

    private static boolean isRoleValid(final Struct role) {
        Role.valueOf(role.getArg(ROLE_STRUCT_CONTENT_POSITION).toString().toUpperCase());
        return role.getName().equals(ROLE_STRUCT_HEAD) && role.getArity() == ROLE_STRUCT_ARITY;
    }

    private static boolean isExpirationInstantValid(final Struct expirationInstant) {
        return expirationInstant.getName().equals(EXPIRATION_INSTANT_STRUCT_HEAD) && expirationInstant.getArity() == EXPIRATION_INSTANT_STRUCT_ARITY &&
                expirationInstant.getArg(EXPIRATION_INSTANT_STRUCT_CONTENT_POSITION) instanceof Number;
    }

    private static boolean isSignatureValid(final Struct signature) {
        return signature.getName().equals(SIGNATURE_STRUCT_HEAD) && signature.getArity() == SIGNATURE_STRUCT_ARITY;
    }

    private static PublicKey publicKeyFromString(final String base64PublicKey)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        final byte[] decodedPublicKey = Base64.decodeBase64(base64PublicKey);
        final X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decodedPublicKey);
        return KeyFactory.getInstance(KeysManager.KEY_INSTANCE).generatePublic(keySpec);
    }
}
