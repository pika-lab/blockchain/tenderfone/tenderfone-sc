package org.amaffi.thesis.security.certificate.model;

import org.amaffi.thesis.security.authorization.Role;
import org.amaffi.thesis.security.certificate.interfaces.Certificate;
import org.apache.commons.codec.binary.Base64;

import java.security.PublicKey;
import java.time.Instant;

import static org.amaffi.thesis.security.certificate.syntax.CertificateSyntax.*;

public class ConcreteCertificate implements Certificate {
    private final String entityAddress;
    private final PublicKey publicKey;
    private final Role role;
    private final Instant expirationInstant;
    private final String signature;

    ConcreteCertificate(final String entityAddress, final PublicKey publicKey,
                               final Role role, final Instant expirationInstant, final String signature) {
        this.entityAddress = entityAddress;
        this.publicKey = publicKey;
        this.role = role;
        this.expirationInstant = expirationInstant;
        this.signature = signature;
    }

    @Override
    public String getEntityAddress() {
        return entityAddress;
    }

    @Override
    public PublicKey getPublicKey() {
        return publicKey;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public Instant getExpirationInstant() {
        return expirationInstant;
    }

    @Override
    public String getSignature() {
        return signature;
    }

    @Override
    public String getSignatureOriginalContent() {
        final String base64PublicKey = Base64.encodeBase64String(getPublicKey().getEncoded());
        return getEntityAddress() + base64PublicKey + getRole().toString().toLowerCase() + getExpirationInstant().getEpochSecond();
    }

    @Override
    public String toString() {
        return CERTIFICATE_HEAD + "(" +
                ENTITY_ADDRESS_STRUCT_HEAD + "(" + entityAddress + ")," +
                PUBLIC_KEY_STRUCT_HEAD + "('" + Base64.encodeBase64String(publicKey.getEncoded()) + "')," +
                ROLE_STRUCT_HEAD + "(" + role.toString().toLowerCase() + ")," +
                EXPIRATION_INSTANT_STRUCT_HEAD + "(" + expirationInstant.getEpochSecond() + ")," +
                SIGNATURE_STRUCT_HEAD + "('" + signature + "')"
                + ")";
    }
}
