package org.amaffi.thesis.security;

import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.transaction.interfaces.Transaction;
import org.amaffi.thesis.transaction.interfaces.TransactionEntry;
import org.amaffi.thesis.transaction.model.TransactionFactory;
import org.amaffi.thesis.util.BlockchainManager;
import org.amaffi.thesis.util.CommunicationUtilities;
import org.amaffi.thesis.util.TimeManager;
import org.amaffi.thesis.util.Utilities;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TransactionCopiesManager {
    private static final int TRANSACTION_TIMEOUT_SECONDS = 5;
    private static final float ACCEPTANCE_RATIO = 0.67f;

    private static final TransactionCopiesManager INSTANCE = new TransactionCopiesManager();

    private final Map<String, TransactionEntry> transactionsCopies;

    private TransactionCopiesManager() {
        transactionsCopies = new HashMap<>();
    }

    public static TransactionCopiesManager getInstance() {
        return INSTANCE;
    }

    public void validateCopy(final Transaction transaction) {
        final String transactionHash =
                Utilities.sha256(transaction.getSenderAddress() + transaction.getBody() + transaction.getNonce());
        if(!transactionsCopies.containsKey(transactionHash)) {
            transactionsCopies.put(transactionHash, TransactionFactory.buildTransactionEntry(transaction));
        } else {
            transactionsCopies.get(transactionHash).validateCopy(transaction);
        }
    }

    public boolean areCopiesEnough(final Transaction transaction) {
        final String transactionHash =
                Utilities.sha256(transaction.getSenderAddress() + transaction.getBody() + transaction.getNonce());
        if(!transactionsCopies.containsKey(transactionHash)) {
            return false;
        }

        final TransactionEntry entry = transactionsCopies.get(transactionHash);
        final float ratio = (float) entry.getValidCopiesNumber() / BlockchainManager.getInstance().getValidatorsNumber();
        if(ratio > ACCEPTANCE_RATIO) {
            entry.consume();
            return true;
        } else {
            return false;
        }
    }

    public void removedExpiredTransactionCopies() {
        final Iterator<Map.Entry<String, TransactionEntry>> iterator = transactionsCopies.entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<String, TransactionEntry> entry = iterator.next();
            final TransactionEntry transactionEntry = entry.getValue();
            final long now = TimeManager.getInstance().getGlobalTime().getEpochSecond();
            if (now - transactionEntry.getCreationTime() > TRANSACTION_TIMEOUT_SECONDS) {
                if(!transactionEntry.hasBeenConsumed()) {
                    CommunicationUtilities.auditTransaction(transactionEntry.getTransaction(), ErrorType.FORGED_TRANSACTION);
                }
                iterator.remove();
            }
        }
    }
}
