package org.amaffi.thesis.security.certificate.syntax;

public final class CertificateSyntax {
    public static final String CERTIFICATE_HEAD = "certificate";
    public static final Integer CERTIFICATE_ARITY = 5;

    public static final String ENTITY_ADDRESS_STRUCT_HEAD = "entity_address";
    public static final String PUBLIC_KEY_STRUCT_HEAD = "public_key";
    public static final String ROLE_STRUCT_HEAD = "role";
    public static final String EXPIRATION_INSTANT_STRUCT_HEAD = "expiration_instant";
    public static final String SIGNATURE_STRUCT_HEAD = "signature";

    public static final Integer ENTITY_ADDRESS_STRUCT_ARITY = 1;
    public static final Integer PUBLIC_KEY_STRUCT_ARITY = 1;
    public static final Integer ROLE_STRUCT_ARITY = 1;
    public static final Integer EXPIRATION_INSTANT_STRUCT_ARITY = 1;
    public static final Integer SIGNATURE_STRUCT_ARITY = 1;

    public static final Integer ENTITY_ADDRESS_STRUCT_POSITION = 0;
    public static final Integer PUBLIC_KEY_STRUCT_POSITION = 1;
    public static final Integer ROLE_STRUCT_POSITION = 2;
    public static final Integer EXPIRATION_INSTANT_STRUCT_POSITION = 3;
    public static final Integer SIGNATURE_STRUCT_POSITION = 4;

    public static final Integer ENTITY_ADDRESS_STRUCT_CONTENT_POSITION = 0;
    public static final Integer PUBLIC_KEY_STRUCT_CONTENT_POSITION = 0;
    public static final Integer ROLE_STRUCT_CONTENT_POSITION = 0;
    public static final Integer EXPIRATION_INSTANT_STRUCT_CONTENT_POSITION = 0;
    public static final Integer SIGNATURE_STRUCT_CONTENT_POSITION = 0;

    private CertificateSyntax() {}
}
