package org.amaffi.thesis.security.certificate.interfaces;

import org.amaffi.thesis.security.authorization.Role;
import org.amaffi.thesis.security.interfaces.SignedEntity;

import java.security.PublicKey;
import java.time.Instant;

public interface Certificate extends SignedEntity {
    String getEntityAddress();
    PublicKey getPublicKey();
    Role getRole();
    Instant getExpirationInstant();
}
