package org.amaffi.thesis.security.authorization;

public enum Role {
    ROOT, NODE, USER, ANY
}
