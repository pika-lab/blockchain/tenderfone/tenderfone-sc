package org.amaffi.thesis.security.certificate.model;

import org.amaffi.thesis.abci.request.RestClient;
import org.amaffi.thesis.security.certificate.interfaces.Certificate;
import org.amaffi.thesis.util.TimeManager;

public final class CertificateManager {
    private static final CertificateManager INSTANCE = new CertificateManager();

    private Certificate appCertificate;

    private CertificateManager() {}

    public static CertificateManager getInstance() {
        return INSTANCE;
    }

    public Certificate getAppCertificate() {
        if(TimeManager.getInstance().getGlobalTime().getEpochSecond() >
                appCertificate.getExpirationInstant().getEpochSecond()) {
            try {
                appCertificate = CertificateFactory.buildCertificate(RestClient.getInstance().requestCertificate());
            } catch (final Exception exception) {
                exception.printStackTrace();
            }
        }
        return appCertificate;
    }

    public void setAppCertificate(final Certificate appCertificate) {
        if(this.appCertificate == null) {
            this.appCertificate = appCertificate;
        }
    }
}
