package org.amaffi.thesis.security.interfaces;

public interface SignedEntity {
    String getSignature();
    String getSignatureOriginalContent();
}
