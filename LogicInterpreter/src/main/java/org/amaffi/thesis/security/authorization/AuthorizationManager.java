package org.amaffi.thesis.security.authorization;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Var;
import org.amaffi.thesis.exception.transaction.UnauthorizedException;
import org.amaffi.thesis.sc.interfaces.SmartContract;
import org.amaffi.thesis.sc.model.SCManager;
import org.amaffi.thesis.transaction.interfaces.Transaction;
import org.amaffi.thesis.transaction.model.TransactionType;
import org.amaffi.thesis.transaction.syntax.body.InvocationGoal;

import java.util.*;

public final class AuthorizationManager {
    private static final AuthorizationManager INSTANCE = new AuthorizationManager();

    private final Map<String, Set<Role>> permissions;

    private AuthorizationManager() {
        permissions = new HashMap<>();
    }

    public static AuthorizationManager getInstance() {
        return INSTANCE;
    }

    public void addPermissionEntry(final String smartContractAddress, final Set<Role> roles) {
        permissions.put(smartContractAddress, new HashSet<>(roles));
    }

    public void removePermissionEntry(final String smartContractAddress) {
        permissions.remove(smartContractAddress);
    }

    public void assertAuthorization(final Transaction transaction) throws UnauthorizedException {
        final Role senderRole = transaction.getCertificate().getRole();
        if (!senderRole.equals(Role.ROOT) && !transaction.getType().equals(TransactionType.CREATION)) {
            final String senderAddress = transaction.getSenderAddress();
            final Term smartContractAddress = transaction.getBody().getArg(InvocationGoal.TARGET_ADDRESS_POSITION);
            switch (transaction.getType()) {
                case INVOCATION:
                    if (smartContractAddress.isList() || smartContractAddress instanceof Var) {
                        Iterator<? extends Term> iterator;
                        if (smartContractAddress instanceof Var) {
                            iterator = SCManager.getInstance().getAllSmartContracts()
                                    .stream().map(sc -> Term.createTerm(sc.getAddress())).iterator();
                        } else {
                            iterator = ((Struct)smartContractAddress).listIterator();
                        }
                        while (iterator.hasNext()) {
                            final String currentAddress = iterator.next().toString();
                            if (!isSenderOwner(senderAddress, smartContractAddress.toString())
                                    && !isSenderAllowed(senderRole, currentAddress)) {
                                throw new UnauthorizedException(transaction);
                            }
                        }
                    } else {
                        if (!isSenderOwner(senderAddress, smartContractAddress.toString())
                                && !isSenderAllowed(senderRole, smartContractAddress.toString())) {
                            throw new UnauthorizedException(transaction);
                        }
                    }
                    break;
                case UPDATE:
                case DESTRUCTION:
                    if (!isSenderOwner(senderAddress, smartContractAddress.toString())) {
                        throw new UnauthorizedException(transaction);
                    }
                    break;
            }
        }
    }


    private boolean isSenderOwner(final String senderAddress, final String smartContractAddress) throws UnauthorizedException {
        final Optional<SmartContract> target =
                SCManager.getInstance().getSmartContract(smartContractAddress);
        return target.isPresent() && target.get().getOwnerAddress().equals(senderAddress);
    }

    private boolean isSenderAllowed(final Role senderRole, final String smartContractAddress) {
        final Optional<SmartContract> target = SCManager.getInstance().getSmartContract(smartContractAddress);

        if (!target.isPresent()) {
            return false;
        }

        final Set<Role> smartContractPermissions = permissions.get(smartContractAddress);
        return smartContractPermissions.contains(Role.ANY) || smartContractPermissions.contains(senderRole);
    }
}
