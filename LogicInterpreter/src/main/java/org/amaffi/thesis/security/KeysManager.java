package org.amaffi.thesis.security;

import org.amaffi.thesis.exception.transaction.CertificateExpiredException;
import org.amaffi.thesis.exception.transaction.InvalidCertificateSignatureException;
import org.amaffi.thesis.exception.transaction.InvalidTransactionSignatureException;
import org.amaffi.thesis.transaction.interfaces.Transaction;

import java.io.File;
import java.io.IOException;

import org.amaffi.thesis.security.interfaces.SignedEntity;
import org.amaffi.thesis.util.TimeManager;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public final class KeysManager {
    public static final String KEY_INSTANCE = "RSA";

    private static final Integer KEY_SIZE = 2048;
    private static final String CA_PUBLIC_KEY_PATH = File.separator + "resources" + File.separator + "ca_public_key.der";
    private static final String SIGNATURE_ALGORITHM = "SHA256WithRSA";

    private static final KeysManager INSTANCE = new KeysManager();

    private PrivateKey appPrivateKey;
    private PublicKey appPublicKey;
    private PublicKey caPublicKey;

    private KeysManager() {
        try {
            final KeyPairGenerator generator = KeyPairGenerator.getInstance(KEY_INSTANCE);
            generator.initialize(KEY_SIZE);
            final KeyPair keyPair = generator.generateKeyPair();
            appPrivateKey = keyPair.getPrivate();
            appPublicKey = keyPair.getPublic();

            final byte[] caPublicKeyBytes = IOUtils.toByteArray(getClass().getResourceAsStream(CA_PUBLIC_KEY_PATH));
            final X509EncodedKeySpec caPublicKeySpec = new X509EncodedKeySpec(caPublicKeyBytes);
            caPublicKey = KeyFactory.getInstance(KEY_INSTANCE).generatePublic(caPublicKeySpec);
        } catch (final IOException | NoSuchAlgorithmException | InvalidKeySpecException exception) {
            exception.printStackTrace();
        }
    }

    public static KeysManager getInstance() {
        return INSTANCE;
    }

    public PublicKey getAppPublicKey() {
        return appPublicKey;
    }

    public String sign(final String toSign) {
        try {
            final Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
            signature.initSign(appPrivateKey);
            signature.update(toSign.getBytes());
            return Base64.encodeBase64String(signature.sign());
        } catch (final NoSuchAlgorithmException | InvalidKeyException | SignatureException exception) {
            exception.printStackTrace();
        }
        return "";
    }

    public void verify(final Transaction transaction)
            throws CertificateExpiredException, InvalidCertificateSignatureException, InvalidTransactionSignatureException {
        if(TimeManager.getInstance().getGlobalTime().getEpochSecond() >
                transaction.getCertificate().getExpirationInstant().getEpochSecond()) {
            throw new CertificateExpiredException(transaction);
        }
        if(!verifySignature(transaction.getCertificate(), caPublicKey)) {
            throw new InvalidCertificateSignatureException(transaction);
        }
        if(!verifySignature(transaction, transaction.getCertificate().getPublicKey())) {
            throw new InvalidTransactionSignatureException(transaction);
        }
    }

    private boolean verifySignature(final SignedEntity signedEntity, final PublicKey publicKey) {
        try {
            final String signature = signedEntity.getSignature();
            final Signature signatureVerifier = Signature.getInstance(SIGNATURE_ALGORITHM);
            signatureVerifier.initVerify(publicKey);
            signatureVerifier.update(signedEntity.getSignatureOriginalContent().getBytes());
            return signatureVerifier.verify(Base64.decodeBase64(signature));
        } catch (final NoSuchAlgorithmException | SignatureException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return false;
    }
}
