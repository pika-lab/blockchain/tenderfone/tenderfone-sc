package org.amaffi.thesis;

import com.github.jtendermint.jabci.types.*;
import com.google.protobuf.ByteString;

import org.amaffi.thesis.abci.ConsensusPhase;
import org.amaffi.thesis.abci.request.RestClient;
import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.abci.server.ABCIServerListener;
import org.amaffi.thesis.abci.response.ResponseBuilder;
import org.amaffi.thesis.security.TransactionCopiesManager;
import org.amaffi.thesis.security.certificate.interfaces.Certificate;
import org.amaffi.thesis.security.certificate.model.CertificateFactory;
import org.amaffi.thesis.security.certificate.model.CertificateManager;
import org.amaffi.thesis.exception.communication.BadCertificationAuthorityResponseException;
import org.amaffi.thesis.exception.general.AppInternalErrorException;
import org.amaffi.thesis.exception.transaction.TransactionElaborationException;

import org.amaffi.thesis.exception.transaction.MalformedCertificateException;
import org.amaffi.thesis.sc.model.SCManager;
import org.amaffi.thesis.transaction.model.TransactionFactory;
import org.amaffi.thesis.transaction.interfaces.Transaction;
import org.amaffi.thesis.transaction.TransactionValidator;
import org.amaffi.thesis.transaction.result.ConcreteInvocationResult;
import org.amaffi.thesis.transaction.result.TransactionResult;
import org.amaffi.thesis.util.*;

import java.io.IOException;

public final class App implements ABCIServerListener{
    private static App INSTANCE;

    private App() {
        try {
            final String stringCertificate = RestClient.getInstance().requestCertificate();
            final Certificate certificate = CertificateFactory.buildCertificate(stringCertificate);
            CertificateManager.getInstance().setAppCertificate(certificate);
        } catch (final BadCertificationAuthorityResponseException | IOException |
                MalformedCertificateException exception) {
            exception.printStackTrace();
            System.exit(-1);
        }
    }

    public static App getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new App();
        }
        return INSTANCE;
    }

    @Override
    public ResponseInitChain onInitChain(final RequestInitChain requestInitChain) {
        BlockchainManager.getInstance().initialize(requestInitChain.getValidatorsCount());
        return ResponseInitChain.newBuilder().build();
    }

    @Override
    public ResponseCheckTx onCheckTx(final RequestCheckTx requestCheckTx) {
        final String stringTx = requestCheckTx.getTx().toStringUtf8();
        try {
            final Transaction transaction = TransactionFactory.buildTransaction(stringTx);
            TransactionValidator.getInstance().validate(transaction);
            switch (transaction.getType()) {
                case CREATION:
                    SCManager.getInstance().processCreationTransaction(transaction, ConsensusPhase.CHECK_TX);
                    return ResponseBuilder.buildCheckTxResponse();
                case INVOCATION:
                    SCManager.getInstance().processInvocationTransaction(transaction, ConsensusPhase.CHECK_TX);
                    return ResponseBuilder.buildCheckTxResponse();
                case UPDATE:
                    SCManager.getInstance().processUpdateTransaction(transaction, ConsensusPhase.CHECK_TX);
                    return ResponseBuilder.buildCheckTxResponse();
                case DESTRUCTION:
                    SCManager.getInstance().processDestructionTransaction(transaction, ConsensusPhase.CHECK_TX);
                    return ResponseBuilder.buildCheckTxResponse();
                default:
                    return ResponseBuilder.buildCheckTxErrorResponse(ErrorType.APP_ERROR);
            }
        } catch (final TransactionElaborationException exception) {
            exception.printStackTrace();
            exception.getTransaction()
                     .ifPresent(tx -> CommunicationUtilities.auditTransaction(tx, exception.getErrorType()));
            return ResponseBuilder.buildCheckTxErrorResponse(exception.getErrorType());
        } catch (final AppInternalErrorException exception) {
            return ResponseBuilder.buildCheckTxErrorResponse(ErrorType.APP_ERROR);
        }
    }

    @Override
    public ResponseBeginBlock onBeginBlock(final RequestBeginBlock requestBeginBlock) {
        TimeManager.getInstance().updateGlobalTime(requestBeginBlock.getHeader().getTime());
        return ResponseBeginBlock.newBuilder().build();
    }

    @Override
    public ResponseDeliverTx onDeliverTx(final RequestDeliverTx requestDeliverTx) {
        final String stringTx = requestDeliverTx.getTx().toStringUtf8();
        try {
            final Transaction transaction = TransactionFactory.buildTransaction(stringTx);
            TransactionValidator.getInstance().validate(transaction);
            final TransactionResult result;
            switch (transaction.getType()) {
                case CREATION:
                    result = SCManager.getInstance().processCreationTransaction(transaction, ConsensusPhase.DELIVER_TX);
                    break;
                case INVOCATION:
                    if(SCManager.getInstance().isSmartContractPresent(transaction.getSenderAddress())) {
                        TransactionCopiesManager.getInstance().validateCopy(transaction);
                        if (TransactionCopiesManager.getInstance().areCopiesEnough(transaction)) {
                            result = SCManager.getInstance().processInvocationTransaction(transaction, ConsensusPhase.DELIVER_TX);
                        } else {
                            result = new ConcreteInvocationResult(transaction);
                        }
                    } else {
                        result = SCManager.getInstance().processInvocationTransaction(transaction, ConsensusPhase.DELIVER_TX);
                    }
                    break;
                case UPDATE:
                    result = SCManager.getInstance().processUpdateTransaction(transaction, ConsensusPhase.DELIVER_TX);
                    break;
                case DESTRUCTION:
                    result = SCManager.getInstance().processDestructionTransaction(transaction, ConsensusPhase.DELIVER_TX);
                    break;
                default:
                    return ResponseBuilder.buildDeliverTxErrorResponse(ErrorType.APP_ERROR);
            }
            TransactionValidator.getInstance().updateExpectedNonce(transaction.getSenderAddress());
            return ResponseBuilder.buildDeliverTxResponse(result);
        } catch (final TransactionElaborationException exception) {
            exception.getTransaction()
                     .ifPresent(tx -> CommunicationUtilities.auditTransaction(tx, exception.getErrorType()));
            return ResponseBuilder.buildDeliverTxErrorResponse(exception.getErrorType());
        } catch (final AppInternalErrorException exception) {
            return ResponseBuilder.buildDeliverTxErrorResponse(ErrorType.APP_ERROR);
        }
    }

    @Override
    public ResponseEndBlock onEndBlock(final RequestEndBlock requestEndBlock) {
        return ResponseEndBlock.newBuilder().build();
    }

    @Override
    public ResponseCommit onCommit(final RequestCommit requestCommit) {
        SCManager.getInstance().onCommit();
        final String treeRootHash = Utilities.getMerkleTreeRoot(SCManager.getInstance().getAllSmartContracts());
        return ResponseCommit.newBuilder().setData(ByteString.copyFrom(treeRootHash.getBytes())).build();
    }

    @Override
    public ResponseQuery onQuery(final RequestQuery requestQuery) {
        return ResponseBuilder.buildQueryResponse(requestQuery);
    }
}
