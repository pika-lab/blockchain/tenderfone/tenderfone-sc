package org.amaffi.thesis.sc.interfaces;

import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;

public interface SmartContract {
    String getAddress();
    String getOwnerAddress();
    SolveInfo assertTerm(Term toAssert);
    SolveInfo retractTerm(Term toRetract);
    SolveInfo invokeReceive(Struct transaction);
    boolean onStart(Struct onStartGoals);
    Theory getTheory();
}
