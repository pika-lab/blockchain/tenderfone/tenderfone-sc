package org.amaffi.thesis.sc.model;

import alice.tuprolog.*;

import alice.tuprolog.exceptions.InvalidLibraryException;
import alice.tuprolog.exceptions.InvalidTheoryException;
import alice.tuprolog.exceptions.NoSolutionException;
import org.amaffi.thesis.sc.interfaces.SmartContract;
import org.amaffi.thesis.sc.lib.*;
import org.amaffi.thesis.transaction.syntax.TransactionSyntax;
import org.amaffi.thesis.transaction.syntax.body.AckGoal;
import org.amaffi.thesis.transaction.syntax.body.AllowedFunctor;
import org.amaffi.thesis.transaction.syntax.body.SendGoal;

public class ConcreteSmartContract implements SmartContract {
    private static final Integer TIMEOUT = 3000;

    protected final Prolog engine;

    ConcreteSmartContract(final String address, final String ownerAddress, final boolean isDummy) {
        engine = new Prolog();
        try {
            engine.getLibraryManager().loadLibrary(new AddressLibrary(address));
            engine.getLibraryManager().loadLibrary(new OwnerLibrary(ownerAddress));
            engine.getLibraryManager()
                    .loadLibrary(isDummy ? DummyTimeLibrary.class.getName() : TimeLibrary.class.getName());
            engine.getLibraryManager()
                    .loadLibrary(isDummy ? DummySendLibrary.class.getName() : SendLibrary.class.getName());
        } catch (final InvalidLibraryException exception) {
            exception.printStackTrace();
        }
    }

    ConcreteSmartContract(final String address, final String ownerAddress,
                          final Theory theory, final boolean isDummy) throws InvalidTheoryException {
        this(address, ownerAddress, isDummy);
        engine.addTheory(theory);
    }

    @Override
    public String getAddress() {
        final Struct goal = Struct.of(AddressLibrary.FUNCTOR, Var.of("X"));
        try {
            return engine.solve(goal).getBindingVars().get(0).getTerm().toString();
        } catch (final NoSolutionException exception) {
            exception.printStackTrace();
            return "broken";
        }
    }

    @Override
    public String getOwnerAddress() {
        final Struct goal = Struct.of(OwnerLibrary.FUNCTOR, Var.of("X"));
        try {
            return engine.solve(goal).getBindingVars().get(0).getTerm().toString();
        } catch (final NoSolutionException exception) {
            exception.printStackTrace();
            return "broken";
        }
    }

    @Override
    public SolveInfo assertTerm(final Term toAssert) {
        return engine.solve(Struct.of(AllowedFunctor.ASSERT.toString().toLowerCase(), toAssert));
    }

    @Override
    public SolveInfo retractTerm(final Term toRetract) {
        return engine.solve(Struct.of(AllowedFunctor.RETRACT.toString().toLowerCase(), toRetract));
    }

    @Override
    public SolveInfo invokeReceive(final Struct transaction) {
        final String sender = ((Struct) transaction.getArg(TransactionSyntax.SENDER_ADDRESS_STRUCT_POSITION))
                .getArg(TransactionSyntax.SENDER_ADDRESS_STRUCT_CONTENT_POSITION).toString();
        final Struct body = (Struct) transaction.getArg(TransactionSyntax.BODY_STRUCT_POSITION);
        final boolean isAck = body.getName().toUpperCase().equals(AllowedFunctor.ACK.toString());

        if(isAck) {
           final Term goal = Struct.of(AllowedFunctor.ACK.toString().toLowerCase(),
                   body.getArg(AckGoal.TRANSACTION_POSITION), body.getArg(AckGoal.RESULT_POSITION));
           return solveInvocation(sender, goal);
        } else {
            final Term goal = body.getTerm(SendGoal.GOAL_POSITION);
            final SolveInfo solveInfo = solveInvocation(sender, goal);
            final String senderAddress = ((Struct)transaction.getArg(TransactionSyntax.SENDER_ADDRESS_STRUCT_POSITION))
                    .getArg(TransactionSyntax.SENDER_ADDRESS_STRUCT_CONTENT_POSITION).toString();
            if(!solveInfo.isTimeout() && SCManager.getInstance().isSmartContractPresent(senderAddress)) {
                sendAck(transaction, solveInfo);
            }
            return solveInfo;
        }
    }

    @Override
    public boolean onStart(final Struct onStartGoals) {
        for(int i = 0; i < onStartGoals.getArity(); i++) {
            if(engine.solve(onStartGoals.getArg(i), TIMEOUT).isTimeout()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Theory getTheory() {
        return engine.getTheory();
    }

    @Override
    public String toString() {
        return engine.getTheory().toString();
    }

    protected SolveInfo solveInvocation(final String sender, final Term goal) {
        assertTerm(Struct.of("sender", Term.createTerm(sender)));
        final SolveInfo result =  engine.solve(Struct.of("receive", goal), TIMEOUT);
        retractTerm(Struct.of("sender", Var.of("X")));
        return result;
    }

    private void sendAck(final Struct transaction, final SolveInfo solveInfo) {
        try {
            final Term result = solveInfo.isSuccess() ? solveInfo.getSolution() : Term.createTerm("no");
            engine.solve(Struct.of(AllowedFunctor.ACK.toString().toLowerCase(), transaction, result));
        } catch (final NoSolutionException ignored) {
            ignored.printStackTrace();
        }
    }
}
