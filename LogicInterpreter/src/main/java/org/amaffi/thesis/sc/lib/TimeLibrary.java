package org.amaffi.thesis.sc.lib;

import alice.tuprolog.Term;
import alice.tuprolog.Number;
import org.amaffi.thesis.util.TimeManager;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TimeLibrary extends DummyTimeLibrary {
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    @Override
    public boolean delayed_task_2(final Term delay, final Term goal) {
        if (!(delay instanceof Number)) {
            return false;
        }
        long actualDt = ((Number) delay).longValue();
        executor.schedule(() -> getEngine().solve(goal), actualDt, TimeUnit.MILLISECONDS);
        return true;
    }

    @Override
    public boolean when_global_2(final Term globalTime, final Term goal) {
        if (!(globalTime instanceof Number)) {
            return false;
        }
        try {
            final Instant whenToSchedule = Instant.ofEpochSecond(((Number)globalTime).longValue());
            final long delay = Duration.between(TimeManager.getInstance().getGlobalTime(), whenToSchedule).getSeconds() * 1000;
            if (delay < 0) {
                return false;
            }
            executor.schedule(() -> getEngine().solve(goal), delay, TimeUnit.MILLISECONDS);
            return true;
        } catch (final Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean periodic_task_3(final Term delay, final Term period, final Term goal) {
        if (!(delay instanceof Number) || !(period instanceof Number)) {
            return false;
        }
        final long actualDelay = ((Number) delay).longValue();
        final long actualPeriod = ((Number) period).longValue();
        executor.scheduleAtFixedRate(() -> getEngine().solve(goal), actualDelay, actualPeriod, TimeUnit.MILLISECONDS);
        return true;
    }

    @Override
    public boolean whenever_global_3(final Term globalTime, final Term period, final Term goal) {
        if (!(globalTime instanceof Number) || !(period instanceof Number)) {
            return false;
        }
        try {
            final Instant whenToSchedule = Instant.ofEpochSecond(((Number)globalTime).longValue());

            final long actualPeriod = ((Number) period).longValue();
            final long initialDelay = Duration.between(TimeManager.getInstance().getGlobalTime(), whenToSchedule).getSeconds() * 1000;
            if (initialDelay < 0) {
                return false;
            }
            executor.scheduleAtFixedRate(() -> getEngine().solve(goal), initialDelay, actualPeriod, TimeUnit.MILLISECONDS);
            return true;
        } catch (final Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }
}
