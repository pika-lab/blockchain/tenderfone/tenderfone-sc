package org.amaffi.thesis.sc.lib;

import alice.tuprolog.Library;
import alice.tuprolog.Term;

public class OwnerLibrary extends Library {
    public static final String FUNCTOR = "owner_address";

    private final String ownerAddress;

    public OwnerLibrary(final String ownerAddress) {
        this.ownerAddress = ownerAddress;
    }

    public boolean owner_address_1(final Term ownerAddressTerm) {
        ownerAddressTerm.unify(getEngine(), Term.createTerm(ownerAddress));
        return true;
    }
}