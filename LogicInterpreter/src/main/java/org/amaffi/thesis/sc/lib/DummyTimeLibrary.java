package org.amaffi.thesis.sc.lib;

import alice.tuprolog.Library;
import alice.tuprolog.Number;
import alice.tuprolog.Term;
import org.amaffi.thesis.util.TimeManager;

public class DummyTimeLibrary extends Library {

    public boolean current_time_1(final Term time) {
        time.unify(getEngine(), Number.createNumber(TimeManager.getInstance().getGlobalTime().getEpochSecond() + ""));
        return true;
    }

    public boolean delayed_task_2(final Term delay, final Term goal) {
        return true;
    }

    public boolean when_global_2(final Term globalTime, final Term goal) {
        return true;
    }

    public boolean periodic_task_3(final Term delay, final Term period, final Term goal) {
        return true;
    }

    public boolean whenever_global_3(final Term globalTime, final Term period, final Term goal) {
        return true;
    }
}
