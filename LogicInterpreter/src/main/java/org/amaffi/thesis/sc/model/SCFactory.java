package org.amaffi.thesis.sc.model;

import alice.tuprolog.*;
import alice.tuprolog.exceptions.InvalidTheoryException;
import org.amaffi.thesis.sc.interfaces.SmartContract;

class SCFactory {

    private SCFactory() {}

    static SmartContract buildSmartContract(final String address, final String ownerAddress) {
        return new ConcreteSmartContract(address, ownerAddress, false);
    }

    static SmartContract buildSmartContract(final String address, final String ownerAddress, final Theory theory) throws InvalidTheoryException {
        return new ConcreteSmartContract(address, ownerAddress, theory, false);
    }

    static SmartContract buildSmartContract(final SmartContract smartContract) {
        try {
            return new ConcreteSmartContract(smartContract.getAddress(),
                    smartContract.getOwnerAddress(), smartContract.getTheory(), true);
        } catch (final InvalidTheoryException exception) {
            exception.printStackTrace();
            return new ConcreteSmartContract(smartContract.getAddress(), smartContract.getOwnerAddress(), true);
        }
    }

    static SmartContract createAuditor(final String address) {
        return new AuditorSmartContract(address);
    }
}
