package org.amaffi.thesis.sc.lib;

import alice.tuprolog.*;
import alice.tuprolog.exceptions.NoSolutionException;
import org.amaffi.thesis.abci.request.RestClient;
import org.amaffi.thesis.transaction.interfaces.Transaction;
import org.amaffi.thesis.transaction.model.TransactionFactory;
import org.amaffi.thesis.transaction.syntax.TransactionSyntax;
import org.amaffi.thesis.transaction.syntax.body.AllowedFunctor;
import org.amaffi.thesis.util.CommunicationUtilities;
import org.apache.http.HttpResponse;
import org.apache.http.concurrent.FutureCallback;

import java.util.Optional;

public class SendLibrary extends DummySendLibrary implements FutureCallback<HttpResponse> {

    private long smartContractNonce;

    @Override
    public boolean send_2(final Term recipientAddress, final Term toSend) {
        try {
            if(!(recipientAddress.getTerm() instanceof Struct && recipientAddress.isList()) && !(recipientAddress.getTerm() instanceof Var)) {
                return false;
            }
            final Struct goal = Struct.of("address", Var.of("X"));
            final String senderAddress = getEngine().solve(goal).getBindingVars().get(0).getTerm().toString();
            final Struct body = Struct.of(AllowedFunctor.SEND.toString().toLowerCase(), recipientAddress, toSend);
            final Optional<Transaction> transaction = TransactionFactory.buildTransaction(senderAddress, body, smartContractNonce);
            if(transaction.isPresent()) {
                RestClient.getInstance().asyncBroadcast(transaction.get(), this);
                smartContractNonce++;
                return true;
            }
            return false;
        } catch (final NoSolutionException exception) {
            exception.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean ack_2(final Term transactionToAck, final Term result) {
        try {
            final Struct goal = Struct.of("address", Var.of("X"));
            final String senderAddress = getEngine().solve(goal).getBindingVars().get(0).getTerm().toString();
            final Term recipientTerm = ((Struct)transactionToAck).getArg(TransactionSyntax.SENDER_ADDRESS_STRUCT_POSITION);
            final Term recipientAddress = ((Struct)recipientTerm).getArg(TransactionSyntax.SENDER_ADDRESS_STRUCT_CONTENT_POSITION);
            final Struct body = Struct.of(AllowedFunctor.ACK.toString().toLowerCase(), recipientAddress, transactionToAck, result);
            final Optional<Transaction> transaction = TransactionFactory.buildTransaction(senderAddress, body, smartContractNonce);
            if(transaction.isPresent()) {
                RestClient.getInstance().asyncBroadcast(transaction.get(), this);
                smartContractNonce++;
                return true;
            }
            return false;
        }catch (final NoSolutionException exception) {
            exception.printStackTrace();
        }
        return false;
    }

    @Override
    public void completed(final HttpResponse httpResponse) {
        if(!CommunicationUtilities.hasTransactionSucceeded(httpResponse)) {
            smartContractNonce--;
        }
    }

    @Override
    public void failed(final Exception e) {
        smartContractNonce--;
    }

    @Override
    public void cancelled() {
        smartContractNonce--;
    }
}
