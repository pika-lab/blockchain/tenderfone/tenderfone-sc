package org.amaffi.thesis.sc.lib;

import alice.tuprolog.Library;
import alice.tuprolog.Term;

public class AddressLibrary extends Library {
    public static final String FUNCTOR = "address";

    private final String address;

    public AddressLibrary(final String address) {
        this.address = address;
    }

    public boolean address_1(final Term termID) {
        termID.unify(getEngine(), Term.createTerm(address));
        return true;
    }
}
