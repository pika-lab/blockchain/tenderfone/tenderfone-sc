package org.amaffi.thesis.sc.lib;

import alice.tuprolog.Library;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import org.amaffi.thesis.transaction.syntax.TransactionSyntax;
import org.amaffi.thesis.transaction.syntax.body.AllowedFunctor;

public class AuditLibrary extends Library {

    public boolean receive_1(final Term toAudit) {
        final Struct transactionStruct =
                Struct.of(TransactionSyntax.TRANSACTION_HEAD, Term.createTerm("S"), Term.createTerm("T"),
                        Term.createTerm("N"), Term.createTerm("C"), Term.createTerm("SIG"));
        final Struct auditStruct =
                Struct.of(AllowedFunctor.AUDIT.toString().toLowerCase(), transactionStruct, Term.createTerm("ERROR"));
        if(auditStruct.unify(getEngine(), toAudit)) {
            getEngine().solve(Struct.of(AllowedFunctor.ASSERT.toString().toLowerCase(), auditStruct));
            return true;
        }
        return false;
    }
}
