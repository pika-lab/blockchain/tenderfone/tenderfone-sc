package org.amaffi.thesis.sc.model;

import alice.tuprolog.*;
import alice.tuprolog.exceptions.InvalidTheoryException;
import com.google.common.collect.Sets;
import org.amaffi.thesis.abci.ConsensusPhase;
import org.amaffi.thesis.sc.AddressFactory;
import org.amaffi.thesis.sc.interfaces.SmartContract;
import org.amaffi.thesis.security.authorization.AuthorizationManager;
import org.amaffi.thesis.security.authorization.Role;
import org.amaffi.thesis.exception.general.AppInternalErrorException;
import org.amaffi.thesis.exception.transaction.CreationTimeoutException;
import org.amaffi.thesis.exception.transaction.NoIdFoundException;
import org.amaffi.thesis.exception.transaction.InvocationTimeoutException;
import org.amaffi.thesis.security.TransactionCopiesManager;
import org.amaffi.thesis.transaction.interfaces.Transaction;
import org.amaffi.thesis.transaction.result.*;
import org.amaffi.thesis.transaction.syntax.body.*;
import org.amaffi.thesis.util.*;

import java.util.*;

public final class SCManager {
    public static final String AUDITOR_ADDRESS =
            AddressFactory.generateSmartContractAddress("app", -1);

    private static final SCManager INSTANCE = new SCManager();

    private final Map<String, SmartContract> smartContractMap;
    private final Map<String, SmartContract> smartContractMapCopy;

    private SCManager() {
        smartContractMap = new HashMap<>();
        smartContractMapCopy = new HashMap<>();
        smartContractMap.put(AUDITOR_ADDRESS, SCFactory.createAuditor(AUDITOR_ADDRESS));
        AuthorizationManager.getInstance().addPermissionEntry(AUDITOR_ADDRESS, Sets.newHashSet(Role.NODE));
    }

    public static SCManager getInstance() {
        return INSTANCE;
    }

    public boolean isSmartContractPresent(final String address) {
        return smartContractMap.containsKey(address);
    }

    public Optional<SmartContract> getSmartContract(final String address) {
        if(isSmartContractPresent(address)) {
            return Optional.of(SCFactory.buildSmartContract(smartContractMap.get(address)));
        } else {
            return Optional.empty();
        }
    }

    public List<SmartContract> getAllSmartContracts() {
        final List<SmartContract> smartContracts = new ArrayList<>();
        for (final String address : smartContractMap.keySet()) {
            smartContracts.add(SCFactory.buildSmartContract(smartContractMap.get(address)));
        }
        return smartContracts;
    }

    public CreationResult processCreationTransaction(final Transaction transaction, final ConsensusPhase consensusPhase)
            throws CreationTimeoutException, AppInternalErrorException {
        final String senderAddress = transaction.getSenderAddress();
        final Struct body = transaction.getBody();
        final Struct structTheory = (Struct) body.getArg(CreateGoal.THEORY_STRUCT_POSITION);
        final Struct structRoles = (Struct) body.getArg(CreateGoal.ROLES_STRUCT_POSITION);
        final Struct structOnStart = (Struct) body.getArg(CreateGoal.ON_START_STRUCT_POSITION);
        final String smartContractAddress =
                AddressFactory.generateSmartContractAddress(senderAddress, transaction.getNonce());

        final SmartContract smartContract;
        if (structTheory.isCompound() && PrologUtilities.theoryFromStruct(structTheory).isPresent()) {
            final Theory theory = PrologUtilities.theoryFromStruct(structTheory).get();
            try {
                smartContract = SCFactory.buildSmartContract(smartContractAddress, senderAddress, theory);
            } catch (InvalidTheoryException exception) {
                throw new AppInternalErrorException(exception);
            }
        } else {
            smartContract = SCFactory.buildSmartContract(smartContractAddress, senderAddress);
        }

        final Set<Role> roles = new HashSet<>();
        try {
            final Iterator<? extends Term> iterator = ((Struct) structRoles.getArg(0)).listIterator();
            iterator.forEachRemaining(term -> roles.add(Role.valueOf(term.toString().toUpperCase())));
        } catch (final IllegalArgumentException exception) {
            throw new AppInternalErrorException(exception);
        }

        if(!smartContract.onStart(structOnStart)) {
            throw new CreationTimeoutException(transaction);
        }

        if(consensusPhase.equals(ConsensusPhase.DELIVER_TX)) {
            smartContractMap.put(smartContractAddress, smartContract);
            AuthorizationManager.getInstance().addPermissionEntry(smartContractAddress, roles);
        }
        return new ConcreteCreationResult(transaction, smartContractAddress);
    }

    public InvocationResult processInvocationTransaction(final Transaction transaction, final ConsensusPhase consensusPhase)
            throws NoIdFoundException, InvocationTimeoutException, AppInternalErrorException {
        final Map<String, SmartContract> target =
                consensusPhase == ConsensusPhase.CHECK_TX ? smartContractMapCopy : smartContractMap;
        final Struct body = transaction.getBody();
        final AllowedFunctor functor = AllowedFunctor.valueOf(body.getName().toUpperCase());
        final Term termAddress = body.getArg(InvocationGoal.TARGET_ADDRESS_POSITION);
        if(!termAddress.isList() && !(termAddress instanceof Var) && target.containsKey(termAddress.toString())) {
            final String smartContractAddress = termAddress.toString();
            if(functor.equals(AllowedFunctor.AUDIT) && !smartContractAddress.equals(SCManager.AUDITOR_ADDRESS)) {
                throw new NoIdFoundException(transaction);
            }
            return executeReceive(transaction, target.get(smartContractAddress));
        } else {
            Iterator<? extends Term> iterator;
            if(termAddress instanceof Var) {
                iterator = SCManager.getInstance().getAllSmartContracts()
                        .stream().map(sc -> Term.createTerm(sc.getAddress())).iterator();
            } else {
                iterator = ((Struct)termAddress).listIterator();
            }
            while (iterator.hasNext()) {
                final String smartContractAddress = iterator.next().toString();
                executeReceive(transaction, target.get(smartContractAddress));
            }
            return new ConcreteMultiInvocationResult(transaction);
        }
    }

    public UpdateResult processUpdateTransaction(final Transaction transaction, final ConsensusPhase consensusPhase)
            throws NoIdFoundException {
        final Map<String, SmartContract> target =
                consensusPhase == ConsensusPhase.CHECK_TX ? smartContractMapCopy : smartContractMap;
        final String targetAddress = transaction.getBody().getArg(DestroyGoal.TARGET_ADDRESS_POSITION).toString();
        if(target.containsKey(targetAddress)) {
            final SmartContract smartContract = target.get(targetAddress);
            final Struct body = transaction.getBody();
            final AllowedFunctor functor = AllowedFunctor.valueOf(body.getName().toUpperCase());
            final SolveInfo solveInfo = functor.equals(AllowedFunctor.ASSERT) ?
                    smartContract.assertTerm(body.getArg(AssertGoal.GOAL_POSITION)) :
                    smartContract.retractTerm(body.getArg(RetractGoal.GOAL_POSITION));
            return new ConcreteUpdateResult(transaction, solveInfo);
        } else {
            throw new NoIdFoundException(transaction);
        }
    }

    public DestructionResult processDestructionTransaction(final Transaction transaction, final ConsensusPhase consensusPhase)
            throws NoIdFoundException {
        final Map<String, SmartContract> target =
                consensusPhase == ConsensusPhase.CHECK_TX ? smartContractMapCopy : smartContractMap;
        final String targetAddress = transaction.getBody().getArg(DestroyGoal.TARGET_ADDRESS_POSITION).toString();
        if(target.containsKey(targetAddress)) {
            target.remove(targetAddress);
            AuthorizationManager.getInstance().removePermissionEntry(targetAddress);
            return new ConcreteDestructionResult(transaction, targetAddress);
        } else {
            throw new NoIdFoundException(transaction);
        }
    }

    public void onCommit() {
        resetStateCopy();
        TransactionCopiesManager.getInstance().removedExpiredTransactionCopies();
    }

    private InvocationResult executeReceive(final Transaction transaction, final SmartContract smartContract)
            throws InvocationTimeoutException {
        final SolveInfo solveInfo = smartContract.invokeReceive(PrologUtilities.transactionToStruct(transaction));
        if (solveInfo.isTimeout()) {
            throw new InvocationTimeoutException(transaction);
        }
        return new ConcreteInvocationResult(transaction, solveInfo);
    }

    private void resetStateCopy() {
        smartContractMapCopy.clear();
        smartContractMap.keySet().forEach(smartContractAddress -> {
            final SmartContract smartContractCopy = SCFactory.buildSmartContract(smartContractMap.get(smartContractAddress));
            smartContractMapCopy.put(smartContractAddress, smartContractCopy);
        });
    }
}
