package org.amaffi.thesis.sc.model;

import alice.tuprolog.*;
import alice.tuprolog.Number;
import alice.tuprolog.exceptions.InvalidLibraryException;
import org.amaffi.thesis.sc.lib.AuditLibrary;
import org.amaffi.thesis.transaction.syntax.TransactionSyntax;
import org.amaffi.thesis.transaction.syntax.body.AllowedFunctor;
import org.amaffi.thesis.transaction.syntax.body.AuditGoal;

import java.lang.Long;

public class AuditorSmartContract extends ConcreteSmartContract {
    private static final String OWNER_ADDRESS = "none";
    private static final String FAIL = "fail";

    private long lastNonce;

    AuditorSmartContract(final String address) {
        super(address, OWNER_ADDRESS, true);
        lastNonce = 0L;
        try {
            engine.getLibraryManager().loadLibrary(AuditLibrary.class.getName());
        } catch (final InvalidLibraryException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public SolveInfo assertTerm(final Term toAssert) {
        return fail();
    }

    @Override
    public SolveInfo retractTerm(final Term toRetract) {
        return fail();
    }

    @Override
    public SolveInfo invokeReceive(final Struct transaction) {
        final Struct senderStruct = (Struct) transaction.getArg(TransactionSyntax.SENDER_ADDRESS_STRUCT_POSITION);
        final String sender = senderStruct.getArg(TransactionSyntax.SENDER_ADDRESS_STRUCT_CONTENT_POSITION).toString();
        final Struct body = (Struct) transaction.getArg(TransactionSyntax.BODY_STRUCT_POSITION);
        final Struct nonceStruct =  (Struct) transaction.getArg(TransactionSyntax.NONCE_STRUCT_POSITION);
        final Long transactionNonce = ((Number)nonceStruct.getArg(TransactionSyntax.NONCE_STRUCT_CONTENT_POSITION)).longValue();
        if(body.getName().toUpperCase().equals(AllowedFunctor.AUDIT.toString()) && lastNonce <= transactionNonce) {
            lastNonce++;
            final Struct goal = Struct.of(AllowedFunctor.AUDIT.toString().toLowerCase(),
                    body.getArg(AuditGoal.TRANSACTION_POSITION),
                    body.getArg(AuditGoal.ERROR_POSITION));
            return solveInvocation(sender, goal);
        } else {
            return fail();
        }
    }

    private SolveInfo fail() {
        return engine.solve(Struct.atom(FAIL));
    }
}
