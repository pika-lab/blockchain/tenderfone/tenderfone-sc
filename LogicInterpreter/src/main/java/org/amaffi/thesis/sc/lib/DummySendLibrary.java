package org.amaffi.thesis.sc.lib;

import alice.tuprolog.*;

public class DummySendLibrary extends Library {

    public boolean send_2(final Term recipient, final Term toSend) {
        return true;
    }

    public boolean ack_2(final Term transactionToAck, final Term result) {
        return true;
    }
}
