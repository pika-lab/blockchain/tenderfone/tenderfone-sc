package org.amaffi.thesis.sc;

import static org.amaffi.thesis.util.Utilities.sha256;

public class AddressFactory {

    private AddressFactory() {}

    public static String generateSmartContractAddress(final String senderAddress, final long nonce) {
        return "sc" + sha256(senderAddress + nonce);
    }
}
