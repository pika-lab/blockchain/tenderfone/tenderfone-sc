package org.amaffi.thesis.transaction.syntax;

public final class TransactionSyntax {
    public static final String TRANSACTION_HEAD = "transaction";
    public static final Integer TRANSACTION_ARITY = 5;

    public static final String SENDER_ADDRESS_STRUCT_HEAD = "sender_address";
    public static final String NONCE_STRUCT_HEAD = "nonce";
    public static final String SIGNATURE_STRUCT_HEAD = "signature";

    public static final Integer SENDER_ADDRESS_STRUCT_ARITY = 1;
    public static final Integer NONCE_STRUCT_ARITY = 1;
    public static final Integer SIGNATURE_STRUCT_ARITY = 1;

    public static final Integer SENDER_ADDRESS_STRUCT_POSITION = 0;
    public static final Integer BODY_STRUCT_POSITION = 1;
    public static final Integer NONCE_STRUCT_POSITION = 2;
    public static final Integer CERTIFICATE_STRUCT_POSITION = 3;
    public static final Integer SIGNATURE_STRUCT_POSITION = 4;

    public static final Integer SENDER_ADDRESS_STRUCT_CONTENT_POSITION = 0;
    public static final Integer NONCE_STRUCT_CONTENT_POSITION = 0;
    public static final Integer SIGNATURE_STRUCT_CONTENT_POSITION = 0;

    private TransactionSyntax() {}
}
