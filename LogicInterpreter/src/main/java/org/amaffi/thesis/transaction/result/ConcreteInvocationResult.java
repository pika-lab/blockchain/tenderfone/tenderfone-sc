package org.amaffi.thesis.transaction.result;

import alice.tuprolog.SolveInfo;
import org.amaffi.thesis.transaction.interfaces.Transaction;

import java.util.Optional;

public class ConcreteInvocationResult extends ConcreteTransactionResult implements InvocationResult {

    private SolveInfo solveInfo;

    public ConcreteInvocationResult(final Transaction transaction, final SolveInfo solveInfo) {
        super(transaction);
        this.solveInfo = solveInfo;
    }

    public ConcreteInvocationResult(final Transaction transaction) {
        this(transaction, null);
    }

    @Override
    public Optional<SolveInfo> getSolveInfo() {
        return Optional.ofNullable(solveInfo);
    }

    @Override
    public boolean isSingleInvocation() {
        return true;
    }
}
