package org.amaffi.thesis.transaction.model;

import alice.tuprolog.Struct;
import org.amaffi.thesis.security.certificate.interfaces.Certificate;
import org.amaffi.thesis.transaction.interfaces.Transaction;

import static org.amaffi.thesis.transaction.syntax.TransactionSyntax.*;

public class ConcreteTransaction implements Transaction {

    private final String senderAddress;
    private final Struct body;
    private final long nonce;
    private final Certificate certificate;
    private final String signature;
    private final TransactionType transactionType;

    ConcreteTransaction(final String senderAddress, final Struct body, final long nonce,
                        final Certificate certificate, final String signature, final TransactionType transactionType) {
        this.senderAddress = senderAddress;
        this.body = body;
        this.nonce = nonce;
        this.certificate = certificate;
        this.signature = signature;
        this.transactionType = transactionType;
    }

    @Override
    public String getSenderAddress() {
        return senderAddress;
    }

    @Override
    public Struct getBody() {
        return body;
    }

    @Override
    public long getNonce() {
        return nonce;
    }

    @Override
    public Certificate getCertificate() {
        return certificate;
    }

    @Override
    public String getSignature() {
        return signature;
    }

    @Override
    public String getSignatureOriginalContent() {
        return getSenderAddress() /*+ getBody()*/ + getNonce() + getCertificate();
    }

    @Override
    public TransactionType getType() {
        return transactionType;
    }

    @Override
    public String toString() {
        return TRANSACTION_HEAD + "(" +
                SENDER_ADDRESS_STRUCT_HEAD + "(" + senderAddress + ")," +
                body + "," +
                NONCE_STRUCT_HEAD + "(" + nonce + ")," +
                certificate + "," +
                SIGNATURE_STRUCT_HEAD + "('" + signature + "')"
                + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Transaction) {
            final Transaction other = (Transaction) obj;
            return senderAddress.equals(other.getSenderAddress()) &&
                    body.equals(other.getBody()) &&
                    nonce == other.getNonce();
        }
        return false;
    }
}
