package org.amaffi.thesis.transaction.result;

import alice.tuprolog.SolveInfo;

import java.util.Optional;

public interface InvocationResult extends TransactionResult {
    Optional<SolveInfo> getSolveInfo();
    boolean isSingleInvocation();
}
