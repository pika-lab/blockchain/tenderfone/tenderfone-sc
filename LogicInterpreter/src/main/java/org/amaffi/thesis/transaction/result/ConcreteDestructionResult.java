package org.amaffi.thesis.transaction.result;

import org.amaffi.thesis.transaction.interfaces.Transaction;

public class ConcreteDestructionResult extends ConcreteTransactionResult implements DestructionResult {

    private final String smartContractAddress;

    public ConcreteDestructionResult(final Transaction transaction, final String smartContractAddress) {
        super(transaction);
        this.smartContractAddress = smartContractAddress;
    }

    @Override
    public String getSmartContractAddress() {
        return smartContractAddress;
    }
}
