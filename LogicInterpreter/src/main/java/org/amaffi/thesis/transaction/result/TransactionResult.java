package org.amaffi.thesis.transaction.result;

import org.amaffi.thesis.transaction.interfaces.Transaction;

public interface TransactionResult {
    Transaction getTarget();
}
