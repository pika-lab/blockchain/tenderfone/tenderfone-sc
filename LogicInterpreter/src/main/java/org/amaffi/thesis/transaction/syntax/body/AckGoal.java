package org.amaffi.thesis.transaction.syntax.body;

public final class AckGoal extends InvocationGoal {
    public static final Integer TRANSACTION_POSITION = 1;
    public static final Integer RESULT_POSITION = 2;

}
