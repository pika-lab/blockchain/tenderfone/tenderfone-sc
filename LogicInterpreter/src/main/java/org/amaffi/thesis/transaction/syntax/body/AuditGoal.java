package org.amaffi.thesis.transaction.syntax.body;

public final class AuditGoal extends InvocationGoal {
    public static final Integer TRANSACTION_POSITION = 1;
    public static final Integer ERROR_POSITION = 2;
}
