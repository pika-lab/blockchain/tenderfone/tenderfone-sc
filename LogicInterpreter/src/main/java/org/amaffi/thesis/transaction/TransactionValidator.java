package org.amaffi.thesis.transaction;

import org.amaffi.thesis.exception.transaction.*;
import org.amaffi.thesis.sc.model.SCManager;
import org.amaffi.thesis.security.authorization.AuthorizationManager;
import org.amaffi.thesis.security.authorization.Role;
import org.amaffi.thesis.transaction.interfaces.Transaction;
import org.amaffi.thesis.security.KeysManager;

import java.util.HashMap;
import java.util.Map;

public final class TransactionValidator {
    private static final TransactionValidator INSTANCE = new TransactionValidator();

    private final Map<String, Long> expectedNonces;

    private TransactionValidator() {
        expectedNonces = new HashMap<>();
    }

    public static TransactionValidator getInstance() {
        return INSTANCE;
    }

    public synchronized void validate(final Transaction transaction)
            throws TransactionElaborationException {

        KeysManager.getInstance().verify(transaction);

        if(!isNonceValid(transaction)) {
            throw new BadNonceException(transaction);
        }

        if(SCManager.getInstance().isSmartContractPresent(transaction.getSenderAddress()) &&
                !transaction.getCertificate().getRole().equals(Role.NODE)) {
            throw new ForgedTransactionException(transaction);
        }

        AuthorizationManager.getInstance().assertAuthorization(transaction);
    }

    public void updateExpectedNonce(final String address) {
        expectedNonces.put(address,
                expectedNonces.getOrDefault(address, 0L) + 1);
    }

    private boolean isNonceValid(final Transaction transaction) {
        return expectedNonces.getOrDefault(transaction.getSenderAddress(), 0L) == transaction.getNonce();
    }
}
