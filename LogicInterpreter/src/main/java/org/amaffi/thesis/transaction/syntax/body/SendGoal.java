package org.amaffi.thesis.transaction.syntax.body;

public final class SendGoal extends InvocationGoal {
    public static final Integer GOAL_POSITION = 1;
}
