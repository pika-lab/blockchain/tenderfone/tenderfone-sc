package org.amaffi.thesis.transaction.syntax.body;

public enum AllowedFunctor {
    CREATE(3), ASSERT(2), RETRACT(2), SEND(2), ACK(3), AUDIT(3),  DESTROY(1);

    private final Integer arity;

    AllowedFunctor(final Integer arity) {
        this.arity = arity;
    }

    public Integer getArity() {
        return arity;
    }
}
