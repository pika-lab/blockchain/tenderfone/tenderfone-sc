package org.amaffi.thesis.transaction.result;

import alice.tuprolog.SolveInfo;
import org.amaffi.thesis.transaction.interfaces.Transaction;

import java.util.Optional;

public class ConcreteMultiInvocationResult extends ConcreteTransactionResult implements InvocationResult {

    public ConcreteMultiInvocationResult(Transaction transaction) {
        super(transaction);
    }

    @Override
    public Optional<SolveInfo> getSolveInfo() {
        return Optional.empty();
    }

    @Override
    public boolean isSingleInvocation() {
        return false;
    }
}
