package org.amaffi.thesis.transaction.syntax.body;

public final class CreateGoal {
    public static final String THEORY_STRUCT_HEAD = "theory";
    public static final String NO_THEORY_STRUCT_HEAD = "no_theory";
    public static final String ROLES_STRUCT_HEAD = "roles";
    public static final String ON_START_STRUCT_HEAD = "on_start";

    public static final Integer THEORY_STRUCT_POSITION = 0;
    public static final Integer ROLES_STRUCT_POSITION = 1;
    public static final Integer ON_START_STRUCT_POSITION = 2;
}
