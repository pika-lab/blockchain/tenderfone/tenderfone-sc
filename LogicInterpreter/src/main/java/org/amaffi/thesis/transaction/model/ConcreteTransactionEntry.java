package org.amaffi.thesis.transaction.model;

import org.amaffi.thesis.transaction.interfaces.Transaction;
import org.amaffi.thesis.transaction.interfaces.TransactionEntry;
import org.amaffi.thesis.util.TimeManager;

import java.util.HashSet;
import java.util.Set;

public class ConcreteTransactionEntry implements TransactionEntry {
    private final Transaction transaction;
    private final Set<String> signatures;
    private final long creationTime;
    private int validCopiesNumber;
    private boolean hasBeenConsumed;

    ConcreteTransactionEntry(final Transaction transaction) {
        this.transaction = transaction;
        signatures = new HashSet<>();
        creationTime = TimeManager.getInstance().getGlobalTime().getEpochSecond();
        validCopiesNumber = 1;
        hasBeenConsumed = false;
    }

    @Override
    public Transaction getTransaction() {
        return transaction;
    }

    @Override
    public int getValidCopiesNumber() {
        return hasBeenConsumed ? -1 : validCopiesNumber;
    }

    @Override
    public long getCreationTime() {
        return creationTime;
    }

    @Override
    public void validateCopy(final Transaction transaction) {
        if(!hasBeenConsumed) {
            final String transactionSignature = transaction.getSignature();
            if (this.transaction.equals(transaction) && !signatures.contains(transactionSignature)) {
                signatures.add(transactionSignature);
                validCopiesNumber++;
            }
        }
    }

    @Override
    public boolean hasBeenConsumed() {
        return hasBeenConsumed;
    }

    @Override
    public void consume() {
        hasBeenConsumed = true;
    }
}
