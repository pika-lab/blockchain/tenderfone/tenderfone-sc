package org.amaffi.thesis.transaction.result;

public interface CreationResult extends TransactionResult {
    String getNewSmartContractAddress();
}
