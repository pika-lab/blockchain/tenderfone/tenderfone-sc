package org.amaffi.thesis.transaction.result;

import org.amaffi.thesis.transaction.interfaces.Transaction;

public abstract class ConcreteTransactionResult implements TransactionResult {
    private final Transaction transaction;

    ConcreteTransactionResult(final Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public Transaction getTarget() {
        return transaction;
    }
}
