package org.amaffi.thesis.transaction.result;

import org.amaffi.thesis.transaction.interfaces.Transaction;

public class ConcreteCreationResult extends ConcreteTransactionResult implements CreationResult {

    private final String newSmartContractID;

    public ConcreteCreationResult(final Transaction transaction, final String newSmartContractID) {
        super(transaction);
        this.newSmartContractID = newSmartContractID;
    }

    @Override
    public String getNewSmartContractAddress() {
        return newSmartContractID;
    }
}
