package org.amaffi.thesis.transaction.result;

public interface DestructionResult extends TransactionResult {
    String getSmartContractAddress();
}
