package org.amaffi.thesis.transaction.interfaces;

public interface TransactionEntry {
    Transaction getTransaction();
    int getValidCopiesNumber();
    long getCreationTime();
    void validateCopy(Transaction transaction);
    boolean hasBeenConsumed();
    void consume();
}
