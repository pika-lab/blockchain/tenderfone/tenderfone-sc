package org.amaffi.thesis.transaction.result;

import alice.tuprolog.SolveInfo;
import org.amaffi.thesis.transaction.interfaces.Transaction;

public class ConcreteUpdateResult extends ConcreteTransactionResult implements UpdateResult {

    private SolveInfo solveInfo;

    public ConcreteUpdateResult(final Transaction transaction, final SolveInfo solveInfo) {
        super(transaction);
        this.solveInfo = solveInfo;
    }

    @Override
    public SolveInfo getSolveInfo() {
        return solveInfo;
    }
}
