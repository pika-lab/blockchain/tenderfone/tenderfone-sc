package org.amaffi.thesis.transaction.model;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Number;

import alice.tuprolog.Var;
import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.security.authorization.Role;
import org.amaffi.thesis.sc.model.SCManager;
import org.amaffi.thesis.security.KeysManager;
import org.amaffi.thesis.security.certificate.interfaces.Certificate;
import org.amaffi.thesis.security.certificate.model.CertificateFactory;
import org.amaffi.thesis.exception.transaction.MalformedCertificateException;
import org.amaffi.thesis.exception.transaction.MalformedTransactionException;
import org.amaffi.thesis.security.certificate.model.CertificateManager;
import org.amaffi.thesis.transaction.interfaces.Transaction;
import org.amaffi.thesis.transaction.interfaces.TransactionEntry;
import org.amaffi.thesis.transaction.syntax.body.*;
import org.amaffi.thesis.util.NonceManager;
import org.amaffi.thesis.util.PrologUtilities;

import java.util.Iterator;
import java.util.Optional;

import static org.amaffi.thesis.transaction.syntax.TransactionSyntax.*;

public class TransactionFactory {

    private TransactionFactory() { }

    public static Transaction buildTransaction(final String stringTransaction)
            throws MalformedTransactionException, MalformedCertificateException {
        try {
            final Struct transaction = (Struct) Term.createTerm(stringTransaction);
            if(transaction.getName().equals(TRANSACTION_HEAD) && transaction.getArity() == TRANSACTION_ARITY
                    && areArgumentsValid(transaction)) {
                final Struct senderAddressStruct = (Struct) transaction.getArg(SENDER_ADDRESS_STRUCT_POSITION);
                final Struct body = (Struct) transaction.getArg(BODY_STRUCT_POSITION);
                final Struct nonceStruct = (Struct) transaction.getArg(NONCE_STRUCT_POSITION);
                final String certificateString = transaction.getArg(CERTIFICATE_STRUCT_POSITION).toString();
                final Struct signatureStruct = (Struct) transaction.getArg(SIGNATURE_STRUCT_POSITION);
                final String senderAddress = senderAddressStruct.getArg(SENDER_ADDRESS_STRUCT_CONTENT_POSITION).toString();
                final Long nonce = ((Number) nonceStruct.getArg(NONCE_STRUCT_CONTENT_POSITION)).longValue();
                final Certificate certificate = CertificateFactory.buildCertificate(certificateString);
                final String signature = signatureStruct.getArg(SIGNATURE_STRUCT_CONTENT_POSITION).toString()
                        .replaceAll("'", "");
                final AllowedFunctor allowed = AllowedFunctor.valueOf(body.getName().toUpperCase());
                final TransactionType type = allowed.equals(AllowedFunctor.DESTROY) ?
                        TransactionType.DESTRUCTION : allowed.equals(AllowedFunctor.CREATE) ?
                        TransactionType.CREATION :
                        (allowed.equals(AllowedFunctor.ASSERT) || allowed.equals(AllowedFunctor.RETRACT)) ?
                                TransactionType.UPDATE : TransactionType.INVOCATION;

                return new ConcreteTransaction(senderAddress, body, nonce, certificate, signature, type);
            }
        } catch(final IllegalArgumentException | ClassCastException ignored) {
            ignored.printStackTrace();
        }

        throw new MalformedTransactionException();
    }

    public static Optional<Transaction> buildTransaction(final String sender, final Struct body,
                                                         final long nonce) {
        try {
            final AllowedFunctor functor = AllowedFunctor.valueOf(body.getName().toUpperCase());
            final TransactionType transactionType =
                    functor.equals(AllowedFunctor.CREATE) ? TransactionType.CREATION :
                            functor.equals(AllowedFunctor.DESTROY) ? TransactionType.DESTRUCTION :
                                    (functor.equals(AllowedFunctor.ASSERT) || functor.equals(AllowedFunctor.RETRACT)) ?
                                            TransactionType.UPDATE : TransactionType.INVOCATION;
            final Certificate appCertificate = CertificateManager.getInstance().getAppCertificate();
            final String toSign = sender /*+ body*/ + nonce + appCertificate;
            final String signature = KeysManager.getInstance().sign(toSign);
            return Optional.of(new ConcreteTransaction(sender, body, nonce, appCertificate, signature, transactionType));
        } catch (final IllegalArgumentException exception) {
            return Optional.empty();
        }
    }

    public static Transaction buildAuditTransaction(final Transaction transaction, final ErrorType errorType) {
        final Struct transactionStruct = PrologUtilities.transactionToStruct(transaction);
        final Struct body = Struct.of(AllowedFunctor.AUDIT.toString().toLowerCase(),
                Term.createTerm(SCManager.AUDITOR_ADDRESS),
                transactionStruct,
                Term.createTerm(errorType.toString().toLowerCase()));
        final Certificate appCertificate = CertificateManager.getInstance().getAppCertificate();
        final String appAddress = appCertificate.getEntityAddress();
        final long appNonce = NonceManager.getInstance().getAppNonce();
        final String toSign = appAddress /*+ body*/ + appNonce + appCertificate;
        final String signature = KeysManager.getInstance().sign(toSign);
        return new ConcreteTransaction(appAddress, body, appNonce, appCertificate, signature, TransactionType.INVOCATION);
    }

    public static TransactionEntry buildTransactionEntry(final Transaction transaction) {
        return new ConcreteTransactionEntry(transaction);
    }

    private static boolean areArgumentsValid(final Struct transactionStruct) {
        final Struct senderAddressStruct = (Struct) transactionStruct.getArg(SENDER_ADDRESS_STRUCT_POSITION);
        final Struct bodyStruct = (Struct) transactionStruct.getArg(BODY_STRUCT_POSITION);
        final Struct nonceStruct = (Struct) transactionStruct.getArg(NONCE_STRUCT_POSITION);
        final Struct signatureStruct =  (Struct) transactionStruct.getArg(SIGNATURE_STRUCT_POSITION);
        return isSenderAddressValid(senderAddressStruct) && isBodyValid(bodyStruct) && isNonceValid(nonceStruct)
                && isSignatureValid(signatureStruct);
    }

    private static boolean isSenderAddressValid(final Struct address) {
        return address.getName().equals(SENDER_ADDRESS_STRUCT_HEAD) && address.getArity() == SENDER_ADDRESS_STRUCT_ARITY;
    }

    private static boolean isBodyValid(final Struct body) {
        final AllowedFunctor allowed = AllowedFunctor.valueOf(body.getName().toUpperCase());
        if(allowed.getArity() != body.getArity()) {
            return false;
        }
        switch (allowed) {
            case CREATE:
                return isCreationValid(body);
            case SEND:
            case ASSERT:
            case RETRACT:
                return isInvocationValid(body);
            case DESTROY:
                return isDestructionValid(body);
            case ACK:
                return isAcknowledgeValid(body);
            case AUDIT:
                return isAuditValid(body);
            default:
                return false;
        }
    }

    private static boolean isNonceValid(final Struct nonce) {
        return nonce.getName().equals(NONCE_STRUCT_HEAD) && nonce.getArity() == NONCE_STRUCT_ARITY &&
                nonce.getArg(NONCE_STRUCT_CONTENT_POSITION) instanceof Number;
    }

    private static boolean isSignatureValid(final Struct signature) {
        return signature.getName().equals(SIGNATURE_STRUCT_HEAD) && signature.getArity() == SIGNATURE_STRUCT_ARITY;
    }

    private static boolean isCreationValid(final Struct body) {
        final Struct theory = (Struct) body.getArg(CreateGoal.THEORY_STRUCT_POSITION);
        final Struct roles = (Struct) body.getArg(CreateGoal.ROLES_STRUCT_POSITION);
        final Struct onStart = (Struct) body.getArg(CreateGoal.ON_START_STRUCT_POSITION);

        final String theoryFunctor = theory.getName();
        final boolean isInitialTheoryOk = theoryFunctor.equals(CreateGoal.THEORY_STRUCT_HEAD)
                && theory.getArity() == 1
                && theory.getArg(0).isList();
        final boolean isNoTheoryOk = theoryFunctor.equals(CreateGoal.NO_THEORY_STRUCT_HEAD)
                && !theory.isCompound();
        if (!isInitialTheoryOk && !isNoTheoryOk) {
            return false;
        }

        if(!roles.getName().equals(CreateGoal.ROLES_STRUCT_HEAD) ||
                roles.getArity() != 1 || !roles.getArg(0).isList()) {
            return false;
        }

        final Iterator<? extends Term> iterator = ((Struct) roles.getArg(0)).listIterator();
        iterator.forEachRemaining(role -> Role.valueOf(role.toString().toUpperCase()));

        return onStart.getName().equals(CreateGoal.ON_START_STRUCT_HEAD) && onStart.getArity() == 1;
    }

    private static boolean isInvocationValid(final Struct body) {
        final AllowedFunctor allowed = AllowedFunctor.valueOf(body.getName().toUpperCase());
        final Term addresses = body.getArg(InvocationGoal.TARGET_ADDRESS_POSITION);
        final Struct goal = (Struct) body.getArg(SendGoal.GOAL_POSITION);
        switch (allowed) {
            case ASSERT:
            case RETRACT:
                return !addresses.isCompound() && !goal.isCompound();
            case SEND:
                return (addresses.isList() || addresses instanceof Var) && !goal.isCompound();
            default:
                return false;
        }
    }

    private static boolean isDestructionValid(final Struct body) {
        return !body.getArg(DestroyGoal.TARGET_ADDRESS_POSITION).isCompound();
    }

    private static boolean isAcknowledgeValid(final Struct body) {
        final Struct address = (Struct) body.getArg(AckGoal.TARGET_ADDRESS_POSITION);
        final Struct transaction = (Struct) body.getArg(AckGoal.TRANSACTION_POSITION);
        return !address.isCompound() && areArgumentsValid(transaction);
    }

    private static boolean isAuditValid(final Struct body) {
        final Struct address = (Struct) body.getArg(AuditGoal.TARGET_ADDRESS_POSITION);
        final Struct transaction = (Struct) body.getArg(AuditGoal.TRANSACTION_POSITION);
        final Struct error = (Struct) body.getArg(AuditGoal.ERROR_POSITION);
        ErrorType.valueOf(error.toString().toUpperCase());
        return !address.isCompound() && areArgumentsValid(transaction);
    }
}
