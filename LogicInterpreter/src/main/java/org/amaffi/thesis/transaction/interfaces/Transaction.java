package org.amaffi.thesis.transaction.interfaces;

import alice.tuprolog.Struct;
import org.amaffi.thesis.security.certificate.interfaces.Certificate;
import org.amaffi.thesis.transaction.model.TransactionType;
import org.amaffi.thesis.security.interfaces.SignedEntity;

public interface Transaction extends SignedEntity {
    String getSenderAddress();
    Struct getBody();
    long getNonce();
    Certificate getCertificate();
    TransactionType getType();
}
