package org.amaffi.thesis.transaction.model;

public enum TransactionType {
    CREATION, INVOCATION, UPDATE, DESTRUCTION
}
