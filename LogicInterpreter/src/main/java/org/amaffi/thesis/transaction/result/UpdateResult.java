package org.amaffi.thesis.transaction.result;

import alice.tuprolog.SolveInfo;

public interface UpdateResult extends TransactionResult {
    SolveInfo getSolveInfo();

}
