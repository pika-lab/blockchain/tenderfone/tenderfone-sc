package org.amaffi.thesis;

import org.amaffi.thesis.abci.server.ABCIServer;

public class EntryPoint {
    public static void main(final String[] args) {
        System.out.println("Starting logic interpreter.");

        final ABCIServer server = new ABCIServer(App.getInstance());
        server.start();
    }
}
