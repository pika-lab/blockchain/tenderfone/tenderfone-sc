package org.amaffi.thesis.exception.transaction;

import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.transaction.interfaces.Transaction;

public class InvalidCertificateSignatureException extends TransactionElaborationException {

    public InvalidCertificateSignatureException(final Transaction transaction) {
        super(transaction);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.INVALID_CERTIFICATE_SIGNATURE;
    }
}
