package org.amaffi.thesis.exception.general;

public class AppInternalErrorException extends Exception {
    private final Exception cause;

    public AppInternalErrorException(final Exception cause) {
        this.cause = cause;
    }

    @Override
    public Exception getCause() {
        return cause;
    }
}
