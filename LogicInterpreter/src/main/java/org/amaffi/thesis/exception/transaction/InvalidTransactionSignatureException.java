package org.amaffi.thesis.exception.transaction;

import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.transaction.interfaces.Transaction;

public class InvalidTransactionSignatureException extends TransactionElaborationException {

    public InvalidTransactionSignatureException(final Transaction transaction) {
        super(transaction);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.INVALID_TRANSACTION_SIGNATURE;
    }
}
