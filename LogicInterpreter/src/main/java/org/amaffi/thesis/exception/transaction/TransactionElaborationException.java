package org.amaffi.thesis.exception.transaction;

import org.amaffi.thesis.transaction.interfaces.Transaction;

import java.util.Optional;

public abstract class TransactionElaborationException extends Exception implements TransactionException {
    private final Transaction transaction;

    TransactionElaborationException(final Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public Optional<Transaction> getTransaction() {
        return Optional.ofNullable(transaction);
    }
}
