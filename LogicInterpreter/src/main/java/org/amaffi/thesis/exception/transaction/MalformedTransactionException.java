package org.amaffi.thesis.exception.transaction;

import org.amaffi.thesis.abci.response.ErrorType;

public class MalformedTransactionException extends TransactionElaborationException {

    public MalformedTransactionException() {
        super(null);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.MALFORMED_TRANSACTION;
    }
}
