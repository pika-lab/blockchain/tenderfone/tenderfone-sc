package org.amaffi.thesis.exception.transaction;

import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.transaction.interfaces.Transaction;

public class BadNonceException extends TransactionElaborationException {

    public BadNonceException(final Transaction transaction) {
        super(transaction);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.BAD_NONCE;
    }
}
