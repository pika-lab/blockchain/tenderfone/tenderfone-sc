package org.amaffi.thesis.exception.transaction;

import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.transaction.interfaces.Transaction;

import java.util.Optional;

public interface TransactionException {
    ErrorType getErrorType();
    Optional<Transaction> getTransaction();
}
