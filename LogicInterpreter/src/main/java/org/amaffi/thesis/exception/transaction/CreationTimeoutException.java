package org.amaffi.thesis.exception.transaction;

import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.transaction.interfaces.Transaction;

public class CreationTimeoutException extends TransactionElaborationException {

    public CreationTimeoutException(Transaction transaction) {
        super(transaction);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.CREATION_TIMEOUT;
    }
}
