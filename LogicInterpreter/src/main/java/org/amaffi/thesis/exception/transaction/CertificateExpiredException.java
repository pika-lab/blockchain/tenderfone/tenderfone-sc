package org.amaffi.thesis.exception.transaction;

import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.transaction.interfaces.Transaction;

public class CertificateExpiredException extends TransactionElaborationException {

    public CertificateExpiredException(final Transaction transaction) {
        super(transaction);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.CERTIFICATE_EXPIRED;
    }
}
