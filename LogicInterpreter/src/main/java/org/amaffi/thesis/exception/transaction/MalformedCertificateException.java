package org.amaffi.thesis.exception.transaction;

import org.amaffi.thesis.abci.response.ErrorType;

public class MalformedCertificateException extends TransactionElaborationException {

    public MalformedCertificateException() {
        super(null);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.MALFORMED_CERTIFICATE;
    }
}
