package org.amaffi.thesis.exception.transaction;

import org.amaffi.thesis.abci.response.ErrorType;
import org.amaffi.thesis.transaction.interfaces.Transaction;

public class UnauthorizedException extends TransactionElaborationException {

    public UnauthorizedException(final Transaction transaction) {
        super(transaction);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.UNAUTHORIZED;
    }
}
