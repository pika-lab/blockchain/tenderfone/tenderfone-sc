# Tenderfone

Tenderfone is a permissioned BCT which supports the deployment and execution of
proactive, logic Smart Contracts.

## Summary

The system is conceptually made up of

* a set of **nodes**, connected in a peer-to-peer fashion, which collectively execute a
consensus protocol to behave as a single, coherent machine. They also offer an
environment for the deployment and execution of smart contracts.
* a **certification authority (CA)**, which is in charge of issuing certificates
to both end users and nodes. It preserves the permissioned nature of the system,
which is necessary to uniquely and securely identify each entity and to
detect forged transactions.
* a **client**, meant to mediate the interaction between end users and nodes. More
precisely, it enables end users to create and broadcast transactions to the
system aimed at creating, invoking, or deleting smart contracts.

Due to their two-fold duty, each node is designed and implemented as two different
processes: a *core* process, responsible for what concerns consensus with other
nodes and blockchain management; a *logic interpreter* process, responsible for
managing Smart Contracts throughout their whole life cycle.

Tenderfone relies on [Tendermint](https://tendermint.com/) for what concerns
the core process of each system node, while all the other components can be
found, along with their source code, in the homonymous folders of this
repository. The [LogicInterpreter](LogicInterpreter/) is written in Java; it relies on
[JTendermint](https://github.com/jTendermint/jabci) to interact with the Tendermint
core and uses [tuProlog](http://apice.unibo.it/xwiki/bin/view/Tuprolog/WebHome) as
the logic engine for Smart Contracts. The [CA](CA/) is written in Javascript (Node)
while the [Client](Client/) is
written in Typescript.

## Deployment

The system deployment is still at a prototypal stage. In this section we describe
how to deploy a single node, relying on [Docker](https://www.docker.com/).

### Requirements

* [Docker](https://www.docker.com/)
* [IntelliJ](https://www.jetbrains.com/idea/) (with Maven Integration Plugin)
* [NodeJs](https://nodejs.org/en/) along with [Typescript](http://www.typescriptlang.org/#download-links) (installation through npm recommended)

### Steps

1. Clone this repository
2. Open a shell and pull [Tenderfone's Docker images](https://hub.docker.com/u/tenderfone) from DockerHub
    * `docker pull tenderfone/logic-interpreter:v1`
    * `docker pull tenderfone/tendermint-core:v1`
    * `docker pull tenderfone/certification-authority:v1`
3. Create a custom docker network, named "tenderfone-net"
    * `docker network create tenderfone-net`
4. Launch IntelliJ and open the LogicInterpreter folder as a Maven project
5. In the "Maven" window -> "Lifecycle" section, by running the "package" task the IDE will compile the
project and start three containers in the brand new docker network
    * **li** - container which runs a logic interpreter
    * **core** - container which runs a Tendermint core
    * **ca** - container which runs the certification authority
6. Open a shell in the Client directory and run
    1. `npm install` to install all dependencies
    2. `tsc` to compile the Client source code, from Typescript to pure Javascript
    3. `npm start` to start the Client
7. Open your browser of choice and connect to the Client at **localhost:7098**

To stop and remove the aforementioned containers, just run the "docker:stop"
task in the "Maven" window, under the "Plugins" section.
