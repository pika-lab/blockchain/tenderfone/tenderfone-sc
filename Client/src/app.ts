import * as express from 'express';

import {HomeController} from './controllers';
import {CertificateController} from './controllers';
import {BroadcastTxController} from './controllers';
import {AuditedTxController} from './controllers';
import {SmartContractController} from './controllers';
import {NetworkErrorController} from './controllers';

const app: express.Application = express();
const port: number = 7098;

app.set('views', './views');
app.set('view engine', 'pug');
app.set('json spaces', 4);
app.use(express.static('./'));

app.use('/', HomeController);
app.use('/requestCertificate', CertificateController);
app.use('/broadcastTransaction', BroadcastTxController);
app.use('/badTransactions', AuditedTxController);
app.use('/smartContracts', SmartContractController);
app.use('/networkError', NetworkErrorController);

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}/`);
});
