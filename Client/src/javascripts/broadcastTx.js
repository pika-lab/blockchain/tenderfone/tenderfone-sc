function changeTransactionType() {
	var theoryDiv = document.getElementById("theoryDiv");
	var rolesDiv = document.getElementById("rolesDiv");
	var addressDiv = document.getElementById("addressDiv");
	var onStartDiv = document.getElementById("onStartDiv");
	var goalDiv = document.getElementById("goalDiv");
	var invocationDiv = document.getElementById("invocationDiv");

	var theoryInput = document.getElementById("theoryInput");
	var rolesInput = document.getElementById("rolesInput");
	var onStartInput = document.getElementById("onStartInput");
	var addressInput = document.getElementById("addressInput");
	var goalInput = document.getElementById("goalInput");
	var invocationInput = document.getElementById("invocationInput")

	var selectedIndex = document.getElementById("txTypeComboBox").selectedIndex;
	console.log(selectedIndex);
	if(selectedIndex == 0) {
		theoryDiv.style.display="block"; theoryInput.removeAttribute("disabled");
		rolesDiv.style.display="block"; rolesInput.removeAttribute("disabled");
		onStartDiv.style.display="block"; onStartInput.removeAttribute("disabled");
		addressDiv.style.display="none"; addressInput.disabled = true;
		goalDiv.style.display="none"; goalInput.disabled = true;
		invocationDiv.style.display="none"; invocationInput.disabled = true;
	} else if(selectedIndex == 1) {
		theoryDiv.style.display="none"; theoryInput.disabled = true;
		rolesDiv.style.display="none"; rolesInput.disabled = true;
		onStartDiv.style.display="none"; onStartInput.disabled = true;
		addressDiv.style.display="block"; addressInput.removeAttribute("disabled");
		goalDiv.style.display="block"; goalInput.removeAttribute("disabled");
		invocationDiv.style.display="block"; invocationInput.removeAttribute("disabled");
	} else if(selectedIndex == 2) {
		addressDiv.style.display="block"; addressInput.removeAttribute("disabled");
		goalDiv.style.display="none"; rolesInput.disabled = true;
		theoryDiv.style.display="none"; theoryInput.disabled = true;
		rolesDiv.style.display="none"; goalInput.disabled = true;
		onStartDiv.style.display="none"; onStartInput.disabled = true;
		invocationDiv.style.display="none";invocationInput.disabled = true;
	}
}

function changeInvocationMode() {
	var addressDiv = document.getElementById("addressDiv");
	var selectedIndex = document.getElementById("sendType").selectedIndex;
	if(selectedIndex == 2) {
		addressDiv.style.display="none"; addressInput.disabled = true;
	} else {
		addressDiv.style.display="block"; addressInput.removeAttribute("disabled");
	}
}
