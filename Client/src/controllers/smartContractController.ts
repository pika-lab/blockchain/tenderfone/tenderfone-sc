import { Router, Request, Response } from 'express';
import * as http from 'http';

const router: Router = Router();

const URLall = 'http://localhost:26657/abci_query?data="smartContractAddresses"'
const URLsingle = 'http://localhost:26657/abci_query?data="smartContract'

router.get('/', (req: Request, res: Response) => {
	var request: http.ClientRequest = http.get(URLall, (response) => {
		let data = '';
		response.on('data', (chunk) => {
			data += chunk;
		});
		response.on('end', () => {
			var appResponse: QueryResponse = JSON.parse(data);
			var decodedData: string =
				appResponse.result.response.value ?
				new Buffer(appResponse.result.response.value, 'base64').toString('utf8') :
				"";
			var ids: string[] = decodedData.split('\n');
			res.render('allSmartContracts', {
				title : 'All Smart Contracts',
				ids
			});
		});
	});
	request.on('error', (err) => {
		res.redirect('./networkError');
	});
});

router.get('/:address(sc[a-z0-9]*)', (req: Request, res: Response) => {
	var smartContracAddress = req.params.address;
	var request: http.ClientRequest = http.get(URLsingle + '(' + smartContracAddress + ')"', (response) => {
		let data = '';
		response.on('data', (chunk) => {
			data += chunk;
		});
		response.on('end', () => {
			var appResponse: QueryResponse = JSON.parse(data);
			var smartContractTheory: string =
				appResponse.result.response.code ? 'This smart contract doesn\'t exist' :
				(appResponse.result.response.value ?
					new Buffer(appResponse.result.response.value, 'base64').toString('utf8') :
					'No theory');
			res.render('scTheory', {
				title : 'Smart Contract Info',
				id: smartContracAddress,
				theory : smartContractTheory
			});
		});
	});
	request.on('error', (err) => {
		res.redirect('./networkError');
	});
});

export const SmartContractController: Router = router;
