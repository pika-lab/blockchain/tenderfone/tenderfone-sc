import { Router, Request, Response } from 'express';
import * as http from 'http';
import {CertificateManager} from '../model/certificateManager';
import {KeyManager} from '../model/keyManager';

const router: Router = Router();

const ROLE = "user";
const URL = "http://localhost:3000/generateCertificateJSON?role=" + ROLE + "&publicKey="

router.get('/', (req: Request, res: Response) => {
    if(!CertificateManager.getInstance().getCertificate()) {
        var publicKey = KeyManager.getInstance().getPublicKey().toString('base64');
        var publicKeyParameter = encodeURIComponent(publicKey);
        var request: http.ClientRequest = http.get(URL + publicKeyParameter, (response) => {
            let data = '';
            response.on('data', (chunk) => {
                data += chunk;
            });
            response.on('end', () => {
                var certificate: Certificate = JSON.parse(data);
                CertificateManager.getInstance().setCertificate(certificate);
                res.redirect('/');
            });
        });
        request.on('error', (err) => {
            res.redirect('./networkError');
        });
    } else {
        res.redirect('/');
    }
});

export const CertificateController: Router = router;
