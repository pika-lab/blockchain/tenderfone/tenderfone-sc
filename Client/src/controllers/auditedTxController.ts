import { Router, Request, Response } from 'express';
import * as http from 'http';

const router: Router = Router();

const URL = "http://localhost:26657/abci_query?data=\"auditedTransactions\""

router.get('/', (req: Request, res: Response) => {
	var request: http.ClientRequest = http.get(URL, (response) => {
		let data = '';
		response.on('data', (chunk) => {
			data += chunk;
		});
		response.on('end', () => {
			var appResponse: QueryResponse = JSON.parse(data);
			var decodedData: string =
				appResponse.result.response.value ?
				new Buffer(appResponse.result.response.value, 'base64').toString('utf8') :
				"Empty";
			res.render('scTheory', {
				title : "Bad Transactions",
				theoryTitle : 'List of all audited transactions',
				theory : decodedData
			});
		});
	});
	request.on('error', (err) => {
		res.redirect('./networkError');
	});
});

export const AuditedTxController: Router = router;
