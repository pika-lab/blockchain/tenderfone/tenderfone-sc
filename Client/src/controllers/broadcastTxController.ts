import { Router, Request, Response } from 'express';
import { CertificateManager } from '../model/certificateManager';
import { TransactionBuilder } from '../model/transactionBuilder';

import * as http from 'http';

const router: Router = Router();

const URL = "http://localhost:26657/broadcast_tx_commit?tx=\""

router.get('/', (req: Request, res: Response) => {
    if(CertificateManager.getInstance().getCertificate()) {
        if(req.query.transactionType) {
            var transaction: string | undefined = undefined;
            if(req.query.transactionType == "creation" && req.query.theory && req.query.roles && req.query.onStart) {
                transaction = TransactionBuilder.getInstance().buildCreationTransaction(req.query.theory.toString(), req.query.roles.toString(), req.query.onStart.toString());
            } else if(req.query.transactionType == "invocation" && req.query.invocationType && req.query.scAddress && req.query.goal) {
                transaction = TransactionBuilder.getInstance().buildInvocationTransaction(req.query.invocationType.toString(), req.query.scAddress.toString(), req.query.goal.toString());
            } else if(req.query.transactionType == "destruction" && req.query.scAddress) {
                transaction = TransactionBuilder.getInstance().buildDestructionTransaction(req.query.scAddress.toString());
            }

            if(transaction) {
                var urlTransaction:string = encodeURIComponent(transaction);
                var request: http.ClientRequest = http.get(URL + urlTransaction + "\"", (response) => {
                    let data = '';
                    response.on('data', (chunk) => {
                        data += chunk;
                    });
                    response.on('end', () => {
                        var transactionResponse: TransactionResponse = JSON.parse(data);
                        if(transactionResponse.result.check_tx.code) {
                            res.render('transactionResult/transactionFailed', {
                                title : 'Transaction Result',
                                result: 'Something went wrong with the sent transaction.',
                                error: transactionResponse.result.check_tx.log
                            });
                        } else {
                            TransactionBuilder.getInstance().incrementNonce();
                            console.log(transactionResponse);
                            var decodedData: string = transactionResponse.result.deliver_tx.log == "no." ? "-" : new Buffer(transactionResponse.result.deliver_tx.data, 'base64').toString('utf8');
                            res.render('transactionResult/transactionSucceeded', {
                                title : 'Transaction Result',
                                result: 'Successfully included into block at height '+ transactionResponse.result.height +'!',
                                log: transactionResponse.result.deliver_tx.log,
                                data: decodedData
                            });
                        }

                    });
                });
                request.on('error', (err) => {
                    res.redirect('./networkError');
                });
            } else {
                res.redirect('/broadcastTransaction');
            }
        } else {
            res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
            res.header('Expires', '-1');
            res.header('Pragma', 'no-cache');
            res.render('broadcastTx', { title : 'New Transaction' });
        }
    } else {
        res.redirect('/');
    }
});

export const BroadcastTxController: Router = router;
