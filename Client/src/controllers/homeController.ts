import { Router, Request, Response } from 'express';
import {CertificateManager} from '../model/certificateManager';

const router: Router = Router();

router.get('/', (req: Request, res: Response) => {
  if(!CertificateManager.getInstance().getCertificate()) {
    res.redirect('/requestCertificate');
  } else {
    res.render('home', { title : "Tenderfone" });
  }
});

export const HomeController: Router = router;
