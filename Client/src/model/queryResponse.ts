interface QueryResponse {
	jsonrpc: string,
	id: string,
	result: {
		response: {
			code: string,
			log: string,
			value: string
		}
	}
}
