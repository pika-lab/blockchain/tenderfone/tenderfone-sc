export class CertificateManager {
    private static instance: CertificateManager;
    private certificate: Certificate | undefined;

    private constructor() { }

    static getInstance(): CertificateManager {
        if (!CertificateManager.instance) {
            CertificateManager.instance = new CertificateManager();
        }
        return CertificateManager.instance;
    }

    getCertificate(): Certificate | undefined {
      return this.certificate;
    }

    setCertificate(certificate: Certificate) {
      this.certificate = certificate;
    }
}
