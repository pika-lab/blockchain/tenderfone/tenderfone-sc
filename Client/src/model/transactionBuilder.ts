import * as crypto from 'crypto';

import {CertificateManager} from "./certificateManager";
import {KeyManager} from "./keyManager";

export class TransactionBuilder {
    private static instance: TransactionBuilder;

    private nonce: number;

    private constructor() {
        this.nonce = 0;
    }

    static getInstance(): TransactionBuilder {
        if (!TransactionBuilder.instance) {
            TransactionBuilder.instance = new TransactionBuilder();
        }
        return TransactionBuilder.instance;
    }

    public incrementNonce(): void {
        this.nonce++;
    }

    public buildCreationTransaction(theory: string, roles: string, onStart: string): string | undefined {
        var body: string = "";
        if(theory == "notheory") {
          body = "create(no_theory,roles([" + roles + "]),on_start(" + onStart  + "))";
        } else {
          body = "create(theory([" + theory + "]),roles([" + roles + "]),on_start(" + onStart  + "))";
        }
        return this.buildTransaction(body);
    }

    public buildInvocationTransaction(invocationType: string, scAddresses: string, goal: string): string | undefined {
        scAddresses = invocationType == "broadcast" ? "_" : "[" + scAddresses +"]";
        return this.buildTransaction("send(" + scAddresses + ", " + goal + ")")
    }

    public buildDestructionTransaction(scAddress: string): string | undefined {
        return this.buildTransaction("destroy(" + scAddress + ")")
    }

    private buildTransaction(body: string): string | undefined {
      var certificate = CertificateManager.getInstance().getCertificate();
      if(certificate) {
          const senderAddress: string = certificate.entityAddress;
          const sign: crypto.Signer = crypto.createSign('RSA-SHA256');

          var prologCertificate = "certificate(entity_address(" + certificate.entityAddress +
                                    "),public_key('" + certificate.publicKey + "'),role(" + certificate.role +
                                    "),expiration_instant(" + certificate.expirationInstant +
                                    "),signature('" + certificate.signature + "'))";

          const toSign: string = senderAddress /*+ body*/ + this.nonce + prologCertificate;
          sign.update(toSign);
          const signature: string = sign.sign(KeyManager.getInstance().getPrivateKeyPem(), 'base64');


          var transactionProlog: string = "transaction(sender_address(" + senderAddress + ")," +
                                            body + ",nonce(" + this.nonce + ")," +
                                            prologCertificate + ",signature('" + signature + "'))";
          return transactionProlog;
      }
      return undefined;
    }
}
