interface TransactionResponse {
	jsonrpc: string,
	id: string,
	result: {
		check_tx: {
			code: string,
			log: string
		},
		deliver_tx: {
			data: string,
			log: string
		},
		hash: string,
		height: string
	}
}
