interface Certificate {
  entityAddress: string,
  publicKey: string,
  role: string,
  expirationInstant: number,
  signature: string
}
