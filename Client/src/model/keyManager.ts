import * as NodeRSA from 'node-rsa';

export class KeyManager {
    private static instance: KeyManager = new KeyManager();

	private privateKey: Buffer;
	private publicKey: Buffer;
    private privateKeyPem: string;
    private publicKeyPem: string;

    private constructor() {
        var keyPair = new NodeRSA( { b : 2048} );
        this.publicKey = keyPair.exportKey('pkcs8-public-der');
        this.privateKey =  keyPair.exportKey('pkcs1-der');
        this.publicKeyPem = keyPair.exportKey('pkcs8-public-pem');
        this.privateKeyPem =  keyPair.exportKey('pkcs1-pem');
	}

    static getInstance(): KeyManager {
        return KeyManager.instance;
    }

    public getPublicKey(): Buffer {
      return this.publicKey;
    }

    public getPrivateKey(): Buffer {
		return this.privateKey;
	}

    public getPublicKeyPem(): string {
        return this.publicKeyPem;
    }

    public getPrivateKeyPem(): string {
        return this.privateKeyPem;
    }
}
